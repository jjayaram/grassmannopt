\documentclass[final]{beamer}
\usepackage[scale=1.24]{beamerposter}
\usepackage{amsmath,amssymb,graphicx,amsthm}			% allows us to import images

%-----------------------------------------------------------
% Define the column width and poster size
% To set effective sepwid, onecolwid and twocolwid values, first choose how many columns you want and how much separation you want between columns
% The separation I chose is 0.024 and I want 4 columns
% Then set onecolwid to be (1-(4+1)*0.024)/4 = 0.22
% Set twocolwid to be 2*onecolwid + sepwid = 0.464
%-----------------------------------------------------------

\newlength{\sepwid}
\newlength{\onecolwid}
\newlength{\twocolwid}
\newlength{\threecolwid}
\setlength{\paperwidth}{48in}
\setlength{\paperheight}{36in}
\setlength{\sepwid}{0.024\paperwidth}
\setlength{\onecolwid}{0.22\paperwidth}
\setlength{\twocolwid}{0.464\paperwidth}
\setlength{\threecolwid}{0.708\paperwidth}
\setlength{\topmargin}{-0.5in}
\usetheme{confposter}
\usepackage{exscale}

\usecaptiontemplate{
\small
\structure{\insertcaptionname~\insertcaptionnumber:}
\insertcaption}

%-----------------------------------------------------------
% Define colours (see beamerthemeconfposter.sty to change these colour definitions)
%-----------------------------------------------------------

%\setbeamercolor{block title}{fg=ngreen,bg=white}
%\setbeamercolor{block body}{fg=black,bg=white}
%\setbeamercolor{block alerted title}{fg=white,bg=dblue!70}
%\setbeamercolor{block alerted body}{fg=black,bg=dblue!10}

\setbeamercolor{block title}{fg=dblue,bg=white}
\setbeamercolor{block alerted title}{fg=white,bg=black!60}


\newcommand{\argmin}{\operatornamewithlimits{argmin}}
\newcommand{\argmax}{\operatornamewithlimits{argmax}}

%----------------------------------------------------------------------------------------
%	TITLE SECTION 
%----------------------------------------------------------------------------------------
    
\title{Subspace Learning using Consensus on the \\ Grassmannian Manifold} % Poster title
\author{Jayaraman J. Thiagarajan$^{\dagger}$ and Karthikeyan Natesan Ramamurthy$^{\ddagger}$} % Author(s)

\institute{$^{\dagger}$Lawrence Livermore National Laboratory, $^{\ddagger}$IBM T.J. Watson Research Center } % Institution(s)

%----------------------------------------------------------------------------------------

\begin{document}

\addtobeamertemplate{headline}{} 
{\begin{tikzpicture}[remember picture, overlay]
     \node [anchor=north west, inner sep=3cm]  at (current page.north west)
     {\includegraphics[height=6cm]{llnl.png}};
  \end{tikzpicture}}
  
  \addtobeamertemplate{headline}{} 
{\begin{tikzpicture}[remember picture, overlay]
     \node [anchor=north east, inner sep=3.5cm]  at (current page.north east)
     {\includegraphics[height=4cm]{ibm-logo.jpg}};
  \end{tikzpicture}}

\addtobeamertemplate{block end}{}{\vspace*{2ex}} % White space under blocks
\addtobeamertemplate{block alerted end}{}{\vspace*{2ex}} % White space under highlighted (alert) blocks

\setlength{\belowcaptionskip}{2ex} % White space under figures
\setlength\belowdisplayshortskip{2ex} % White space under equations

\begin{frame}[t] % The whole poster is enclosed in one beamer frame

\begin{columns}[t] % The whole poster consists of three major columns, the second of which is split into two columns twice - the [t] option aligns each column's content to the top

\begin{column}{\sepwid}\end{column} % Empty spacer column

\begin{column}{\onecolwid} % The first column

%----------------------------------------------------------------------------------------
%	ABSTRACT
%----------------------------------------------------------------------------------------

\begin{alertblock}{Abstract}
Embedding techniques attempt to represent similar objects by nearby points in a low-dimensional metric map. Uncertainties in data and the sensitivity to parameter settings, reduce the reliability of the MAP solution obtained by such methods. In this paper, we explore approaches to mitigate this challenge by learning a plurality of diverse subspaces and inferring a consensus on the Grassmannian manifold. Using the proposed approach, we build variants of popular unsupervised and supervised linear embedding algorithms, and show that we can significantly improve their usability in visualization and classification.
\end{alertblock}

%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------

\begin{block}{Introduction}
Low-dimensional embedding algorithms attempt to to obtain representations that disentangle the various degrees of freedom and reject noise.
\begin{itemize}
\item \textit{Issues:} (i) uncertainties in data, (ii) global vs local, (iii) suitability of loss function, (iv) choice of algorithm parameters.
\item MAP solution obtained using even numerically stable algorithms can be quite different from the ``best'' solution. 
\end{itemize}

\vspace{0.3in}
\textbf{Proposed Approach}: Learn a plurality of \textit{diverse} embeddings by exploring the space of possible solutions and compute a consensus embedding.

\vspace{0.3in}
Similar to methods that average multiple hypotheses in a model space - e.g. orthogonal decision trees.

\vspace{0.3in}
A \textbf{Grassmannian manifold}, $\text{Gr}(N,M)$ is a set of $N-$dimensional subspaces in $\mathbb{R}^M$, where each subspace maps to a unique point on the manifold.
\begin{itemize}
\item Distance between two points along the geodesic - $\sum_{i=1}^N\left(\theta_i^2\right)^{\frac{1}{2}}$, $\theta_i$ are the principal angles.
\item \textit{Chordal distance} ($\sqrt{N-\|\mathbf{A}^T\mathbf{B}\|_F^2}$) is more convenient to optimize and widely adopted.
\end{itemize}


\end{block}

%\begin{block}{References}
%\small
%[1] Gregory F. Fasshauer, Meshfree Approximation Methods with MATLAB, \textit{World Scientific Publishing Co.}, 2007. 

%[2] N. Monnig, B. Fornberg, and F. Meyer, ``Inverting non-linear dimensionality reduction with scale-free radial basis interpolation,''\textit{ arXiv
%preprint arXiv:1305.0258}, 2013.
%\end{block}

\tiny
This work was performed under the auspices of the U.S. Department of Energy by Lawrence Livermore National Laboratory under Contract DEAC52- 07NA27344

\normalsize
%\begin{figure}
%\includegraphics[width=0.8\linewidth]{placeholder.jpg}
%\caption{Figure caption}
%\end{figure}

\end{column} % End of the first column

\begin{column}{\sepwid}\end{column} % Empty spacer column

\begin{column}{\threecolwid} 


%----------------------------------------------------------------------------------------
%	IMPORTANT RESULT
%----------------------------------------------------------------------------------------

\begin{alertblock}{Obtaining Multiple Inchoherent Embedding Hypotheses}
We introduce an incoherence constraint to the graph embedding formulation, which chooses an embedding that is structurally different from those chosen so far on the Grassmannian. By exploring a diverse set of ``highly probable'' solutions, we can significantly improve the reliability of embeddings.
\begin{equation}
\nonumber
\text{Given } j \text{ previously inferred hypotheses, the } j+1^{\text{th}} \text{ hypothesis is obtained as } \argmin_{\mathbf{v}} \left( \mathbf{X} \mathbf{L} \mathbf{X}^T + \alpha \sum_{i=1}^j \left( \mathbf{V}_i \mathbf{V}_i^T \right) \right) \mathbf{v} = \lambda \mathbf{X} \mathbf{H} \mathbf{X}^T \mathbf{v}.
\end{equation}
\end{alertblock} 

%----------------------------------------------------------------------------------------

\begin{columns}[t,totalwidth=\twocolwid] % Split up the two columns wide column again

\begin{column}{\onecolwid} % The first column within column 2 (column 2.1)

%----------------------------------------------------------------------------------------
%	MATHEMATICAL SECTION
%----------------------------------------------------------------------------------------

\begin{block}{Grassmannian Averaging}
Given $K$ embedding hypotheses, $\{V_i\}_{i=1}^K$, we compute the consensus embedding closest (in Chordal distance) to all hypotheses on average.
\begin{align}
\nonumber
\mathbf{V} &= \argmin_{\mathbf{V}^T\mathbf{V} = \mathbf{I}} \sum_{i=1}^K w_i \left(N_m-\|\mathbf{V}^T\mathbf{V}_i\|_F^2\right),\\
\nonumber &= \argmin_{\mathbf{V}^T\mathbf{V}= \mathbf{I}} \text{Tr} \left( \mathbf{V}^T \sum_{i=1}^K \left(w_i \mathbf{I} - w_i \mathbf{V}_i \mathbf{V}_i^T \right) \mathbf{V} \right) 
\label{eqn:grass_avg}
\end{align}
\end{block}

\begin{block}{Performance Evaluation}
\textbf{Unsupervised Methods:} Locality Preserving Projections (LPP) and Neighborhood Preserving Embedding (NPE).

\vspace{0.2in}
\textbf{Supervised Methods:} Linear Discriminant Analysis (LDA) and Local Discriminant Embedding (LDE).

\vspace{0.2in}
\textbf{Evaluation Metrics}:
\begin{equation}
\nonumber
precision(i) = \frac{|\Omega_h(i) \cap \Omega_l(i)|}{N_l},
\end{equation}
\begin{equation}
\nonumber
recall(i) = \frac{|\Omega_h(i) \cap \Omega_l(i)|}{N_h},
\end{equation}where $\Omega_h$ and $\Omega_l$ denote the set of neighborhood samples for original and embedded data respectively.
\end{block}

\begin{block}{References}
\small
[1] S. Yan, \textit{et al.}, ``Graph embedding and extensions: a general framework for dimensionality reduction,'' \textit{IEEE TPAMI}, vol. 29, no. 1, pp. 40--51, 2007.

[2] K. Ye and L.-H. Lim, ``Distance between subspaces of different dimensions,'' \textit{arXiv preprint arXiv:1407.0900}, 2014.

\end{block}

%----------------------------------------------------------------------------------------

\end{column} % End of column 2.1

\begin{column}{\sepwid}\end{column} % Empty spacer column

\begin{column}{\twocolwid} % The second column within column 2 (column 2.2)

%----------------------------------------------------------------------------------------
%	RESULTS
%----------------------------------------------------------------------------------------

\begin{block}{Simulation Results for Linear DR}

\vspace{0.5in}
\begin{center}
\includegraphics[width=0.9\linewidth]{unsupall.png}
\end{center}

\vspace{1.1in}
\begin{center}
\includegraphics[width=0.9\linewidth]{supall.png}
\end{center}

\end{block}

%----------------------------------------------------------------------------------------

\end{column} % End of column 2.2

\end{columns} % End of the split of column 2

\end{column} % End of the second column


\end{columns} % End of all the columns in the poster

\end{frame} % End of the enclosing frame
\end{document}
