\documentclass[conference]{article}
\usepackage{spconf, blindtext, graphicx, verbatim, amssymb, amsmath}

\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}

\begin{document}
\ninept 
%
% paper title
% can use linebreaks \\ within to get better formatting as desired
\title{Subspace Learning using Consensus on the Grassmannian Manifold}


% author names and affiliations
% use a multiple column layout for up to three different
% affiliations
\name{Jayaraman J. Thiagarajan$^{\dagger}$ and Karthikeyan Natesan Ramamurthy$^{\ddagger}$}
\address{$\dagger$Lawrence Livermore National Laboratory, $\ddagger$IBM Thomas J.~Watson Research Center}

% make the title area
\maketitle

\begin{abstract}
High-dimensional structure of data can be explored and task-specific representations can be obtained using manifold learning and low-dimensional embedding approaches. However, the uncertainties in data and the sensitivity of the algorithms to parameter settings, reduce the reliability of such representations, and make visualization and interpretation of data very challenging. A natural approach to combat challenges pertinent to data visualization is to use linearized embedding approaches. In this paper, we explore approaches to improve the reliability of linearized, subspace embedding frameworks by learning a plurality of subspaces and computing a geometric mean on the Grassmannian manifold. Using the proposed algorithm, we build variants of popular unsupervised and supervised graph embedding algorithms, and show that we can infer high-quality embeddings, thereby significantly improving their usability in visualization and classification.
\end{abstract}

\begin{keywords}
graph embedding, subspace learning, Grassmannian manifold, visualization.
\end{keywords}

\section{Introduction}
\label{sec:intro}
Low-dimensional representations of high dimensional data are desired for several reasons. If the data generating process has low degrees of freedom, it may be useful to obtain representations that disentangle the various degrees of freedom and reject the noise. This is the goal of several manifold learning algorithms, many of which can be posed using a Graph embedding formulation \cite{yan2007graph}. The representations created by these methods retain either the geometric characteristics or the topology (local neighborhood) or both, depending on the type of the embedding algorithm used. Furthermore, supervised embedding approaches create representations, that improve the discrimination across classes apart from preserving the similarity between within-class samples. 

Some examples of unsupervised graph embedding approaches include Principal Components Analysis (PCA) \cite{dunteman1989principal}, Multi-Dimensional Scaling (MDS) \cite{kruskal1964multidimensional}, ISOMAP \cite{tenenbaum2000global}, Laplacian Eigenmaps (LE) \cite{belkin2001laplacian}, Locality Preserving Projections (LPP) \cite{niyogi2004locality}, and Neighborhood Preserving Embedding (NPE) \cite{he2005neighborhood}. Some well-known supervised embedding approaches include Linear Discriminant Analysis (LDA) \cite{lachenbruch1975discriminant}, Marginal Fisher Analysis (MFA) \cite{yan2007graph}, and Local Discriminant Embedding (LDE) \cite{chen2005local}. Though direct graph embedding approaches such as ISOMAP, LE, and MDS have been successfully used to learn underlying non-linear manifolds, linearized embedding algorithms have been of particular interest to practitioners. Explicitly inferring the linear subspace (e.g. PCA) greatly simplifies the out-of-sample extension procedure. In addition, when compared to direct embeddings where the interpretation of the embedded space is entirely opaque, linearized embeddings can lead to more meaningful data visualization.


%Another primary use of low-dimensional representations is in information visualization and retrieval, where the dimensionality of embedding is either $2$ or $3$ \cite{XXX}.

%When computing representations for a particular type of data, not all graph embedding algorithms will be suitable, and each algorithm may also have several parameters to tune. 

The success of graph embedding depends not only on the parameters such as the embedding dimension or the neighborhood graph that needs to be carefully designed, but also the suitability of the embedding approach to the data under consideration. Furthermore, most algorithms only have global parameters, whereas the topological characteristics of data could vary locally. An example of this is when the data is perfectly sampled from a smooth manifold of constant intrinsic dimensionality, the embedding obtained may not be meaningful if its dimensionality does not match the intrinsic dimension of the manifold. This is common in visualization applications, where the embedding dimension is constrained to be $2$ or $3$. An alternative approach proposed in \cite{venna2010information} attempts to alleviate this challenge by posing embedding inference as an information retrieval problem, and constructs a direct $2-D$ embedding that optimizes a function of neighborhood precision and recall. Similar issues exist with supervised embedding approaches as well.

%representations are: (a) the manifold may not be sampled uniformly and sufficiently, (b) the data samples do not lie perfectly in the manifold and are corrupted with noise, which is possibly local and anisotropic, and (c) the manifold has varying intrinsic dimensionality. 

%Under these conditions, the manifold learning approaches cannot provide an embedding that satisfy the geometric and topological constraints.
%Note that this can be applied to any of the unsupervised and supervised approaches mentioned above since all of them evaluate low-dimensional subspaces that either directly constitutes the embedding or can be used to obtain the embedding.

In this paper, we propose approaches that improve the performance of linearized, unsupervised and supervised graph embedding methods, by computing an ensemble of incoherent linear subspaces for the data. Since the subspaces obtained lie on a Grassmannian manifold, a consensus subspace can be inferred on the Grassmannian. The final embedding will be obtained by projecting the data onto the consensus subspace. Using the proposed technique, we build variants of some of the popular unsupervised and supervised embedding algorithms and evaluate their performance on several datasets. Experiment results show that the proposed algorithm can significantly improve the quality of linearized subspace learning methods, and can also impact their performance in conventional tasks such as classification, and visualization for data exploration.


\section{Theory and Background}
\label{sec:theory}

\subsection{Graph Embedding}
\label{sec:graphEmb}
Let us consider a set of samples of $T$ samples denoted by $\mathbf{X} = [\mathbf{x}_1, \mathbf{x}_2, \ldots, \mathbf{x}_T]$, where $\mathbf{x}_i \in \mathbb{R}^M$. For supervised embedding, we will also assume that there are $C$ classes with labels $\{1,\ldots,C\}$. The class $c$ has $T_c$ samples and $\mathcal{I}_c$ contains the indices of samples in that class. The goal of embedding approaches is to find a mapping function $v: \mathbf{x} \rightarrow \mathbf{y}$, where $\mathbf{y} \in \mathbb{R}^N$ and $N \ll M$. Direct methods obtain the embedding $\mathbf{y}$ directly using an implicitly defined $v$, whereas linearized methods obtain $v$ as the linear subspace whose orthonormal basis is defined as $\mathbf{V} \in \mathbb{R}^{M \times N}$, and hence $\mathbf{y} = \mathbf{V}^T \mathbf{x}$.

We will define a similarity graph $G$ that comprises the vertex set $\mathbf{X}$ and the similarity between each of the vertices encoded in the symmetric adjacency matrix $\mathbf{W} \in \mathbb{R}^{T \times T}$. With the exception of NPE, for all the linearized methods we consider, the Laplacian matrix is defined as $\mathbf{L} = \mathbf{D}-\mathbf{W}$, where $\mathbf{D}$ is the diagonal degree matrix of the graph with $D_{ii} = \sum_{i \neq j} W_{ij}$. In NPE, $\mathbf{L}$ is directly defined as $(\mathbf{I}-\mathbf{W})^T (\mathbf{I}-\mathbf{W})$. Furthermore, we create a penalty matrix $\mathbf{H}$, which imposes a constraint on  $\mathbf{Y}^T \mathbf{H} \mathbf{Y} = \mathbf{I}$, where $\mathbf{I}$ is the identity matrix. For example, with LPP, $\mathbf{H}$ is chosen to be the degree matrix $\mathbf{D}$, whereas with NPE it is chosen to be $\mathbf{I}$. In supervised dimensionality reduction, $\mathbf{H}$ is chosen to be the penalty Laplacian, $\mathbf{L} = \mathbf{D}'-\mathbf{W}'$, where $\mathbf{D}'$ and $\mathbf{W}'$ are respectively the degree and adjacency matrices of the penalty graphs. The forms of $\mathbf{L}$ and $\mathbf{H}$ for various graph embeddings can be found in \cite{yan2007graph}.

The embedding directions $\mathbf{V}$ with linearized methods can be optimized as
\begin{eqnarray}
\mathbf{V} = \argmin_{\mathbf{V}^T \mathbf{X} \mathbf{H} \mathbf{X}^T \mathbf{V} = \mathbf{I}} \text{Tr}(\mathbf{V}^T \mathbf{X} \mathbf{L} \mathbf{X}^T \mathbf{V}).
\label{eqn:graphemb_LE}
\end{eqnarray} This optimization can be carried out by choosing the $N$ eigen vectors corresponding to the minimum eigen values of the generalized eigen decomposition (GED),
\begin{eqnarray}
\mathbf{X} \mathbf{L} \mathbf{X}^T \mathbf{v} = \lambda \mathbf{X} \mathbf{H} \mathbf{X}^T \mathbf{v},
\label{eqn:graphemb_LE_GEVD}
\end{eqnarray} and the low-dimensional embedding can be computed as $\mathbf{Y} = \mathbf{V}^T\mathbf{X}$. Note that $\mathbf{V}$ obtained with the GED is not guaranteed to contain only orthonormal columns.

Note that the eigen decomposition method is typically suited for unsupervised methods since $\mathbf{H}$ will truly represent a constraint. In supervised approaches, ideally we would like to compute embeddings that simultaneously maximize $\text{Tr}(\mathbf{V}^T \mathbf{X} \mathbf{H} \mathbf{X}^T \mathbf{V})$, while minimizing $\text{Tr}(\mathbf{V}^T \mathbf{X} \mathbf{L} \mathbf{X}^T \mathbf{V})$. In these cases, a provable globally optimal solution can be obtained for graph embedding by including an additional constraint $\mathbf{V}^T \mathbf{V} = \mathbf{I}$. The optimal embedding can be now obtained by maximizing the trace ratio
\begin{eqnarray}
\mathbf{V} = \argmax_{\mathbf{V}^T \mathbf{V} = \mathbf{I}} \frac{\text{Tr}(\mathbf{V}^T \mathbf{X} \mathbf{H} \mathbf{X}^T \mathbf{V})} {\text{Tr}(\mathbf{V}^T \mathbf{X} \mathbf{L} \mathbf{X}^T \mathbf{V})}.
\label{eqn:trratio}
\end{eqnarray} Approaches such as iterative trace ratio (ITR) maximization  or decomposed Newton's method (DNM) can be used for this purpose and it has been shown that trace ratio maximization performs better than generalized eigen decomposition in supervised dimensionality reduction \cite{wang2007trace,jia2009trace}.

\subsection{Grassmannian Manifolds}
\label{sec:grassmann}
A Grassmannian manifold, $\text{Gr}(N,M)$ is a set of $N-$dimensional subspaces in $\mathbb{R}^M$, where each subspace maps to a unique point on the manifold \cite{harris1992algebraic}. Each point in $\text{Gr}(N,M)$ can be conveniently represented using an $M \times N$ orthonormal matrix which forms a basis for that subspace. Given two points in a Grassmannian, represented by their orthonormal bases, $\mathbf{A}$ and $\mathbf{B}$ of size $M \times N$, there are several distance measures that can be computed between them. The distance measured along the geodesic is the Grassmann distance and can be computed by decomposing $\mathbf{A}^T\mathbf{B}$ using its SVD and obtaining $\sum_{i=1}^N\left(\theta_i^2\right)^{\frac{1}{2}}$. Here, $\theta_i$ denotes a principal angle and is obtained as $\cos^{-1} \sigma_i$, where $\sigma_i$ is the corresponding singular value \cite{ye2014distance}. 

%In our proposed approach, we will learn a plurality of subspaces and treat them as points on the Grassmannian manifold.

In general, Grassmann distances are difficult to optimize with, and hence the chordal distance \cite{ye2014distance}, given as $\sqrt{N-\|\mathbf{A}^T\mathbf{B}\|_F^2}$ is widely adopted in lieu of the true geodesic distance \cite{sun2007further,wang2006subspace}, among several others. The chordal distance is also referred to as the symmetric directional distance. The idea of distance between the subspaces can also be extended to the case when we have two subspaces of different dimensions \cite{ye2014distance}. For example, suppose $\mathbf{A} \in \mathbb{R}^{M \times N_1}$ and $\mathbf{B} \in \mathbb{R}^{M \times N_2}$, the chordal distance is given by $\sqrt{\max(N_1,N_2)-\|\mathbf{A}^T\mathbf{B}\|_F^2}$. If we use chordal distances, incoherence between subspaces corresponds to large distances and vice-versa. For a given distance metric, the mean of two subspaces is defined as the subspace that has the minimum sum of squared distance with the given subspaces. The mean of two subspaces of different dimensions is known for several metrics including the Grassmann and the chordal distances \cite{ye2014distance}. However, with more than two subspaces, it is much easier to use chordal distances to compute the mean, and also it is flexible to be coupled with other constraints, and hence we will focus on this distance measure in our algorithm.

\section{Proposed Algorithm}
\label{sec:algo}
Our proposed approach aims to improve the reliability of linearized unsupervised and supervised embedding algorithms by learning a plurality of subspaces with incoherence constraints between them and finding the mean subspace on the Grassmannian. The final embedding will be obtained by projecting the data onto the mean subspace.

We will begin by describing the algorithm for the unsupervised case. As described in Section \ref{sec:graphEmb}, we will begin with the data matrix $\mathbf{X}$, the Laplacian $\mathbf{L}$ and the constraint matrix $\mathbf{H}$. We aim to construct $K$ low-dimensional projections $\{\mathbf{V}_i\}_{i=1}^{K}$, each with dimensions $M \times N_i$ where $N_i < M$, that satisfy the objective in (\ref{eqn:graphemb_LE}), and are far from each other in the Grassmannian. Here, $\mathbf{V}_i$ are assumed to contain orthonormal columns.

\begin{figure*}[ht]
\begin{minipage}[b]{0.32\linewidth}
 \centering
 \centerline{(a) Faces dataset.}
\includegraphics[width = 6cm]{olivetti_d5.png}
\end{minipage}
\hfill
\begin{minipage}[b]{0.32\linewidth}
 \centering
 \centerline{(b) Seawater dataset.}
\includegraphics[width = 6cm]{seaWater_d3.png}
\end{minipage}
\hfill
\begin{minipage}[b]{0.32\linewidth}
 \centering
 \centerline{(c) Ecoli dataset.}
\includegraphics[width = 6cm]{ecoli_d2.png}
\end{minipage}
\vfill
\vspace{0.1in}
\begin{minipage}[b]{0.32\linewidth}
 \centering
 \centerline{(d) Face Videos dataset.}
\includegraphics[width = 6cm]{FaceVideo_d5.png}
\end{minipage}
\hfill
\begin{minipage}[b]{0.32\linewidth}
 \centering
 \centerline{(e) NPE Embedding.}
\includegraphics[width = 6.1cm]{NPE_ecoli.png}
\end{minipage}
\hfill
\begin{minipage}[b]{0.32\linewidth}
 \centering
 \centerline{(f) Proposed Embedding.}
\includegraphics[width = 6.1cm]{NPEGM_ecoli.png}
\end{minipage}
\caption{Unsupervised Graph Embedding: (a)-(d) Mean precision - Mean Recall curves for different datasets; (e)-(f) $2$-D embeddings for the UCI ecoli datasets obtained using NPE and the proposed variant.}
\label{Fig:unsup}
\end{figure*}

\subsection{Obtaining Multiple Incoherent Subspaces}
\label{sec:incoh_subspace_comp}
Let us assume that we have already computed $j$ subspaces and we wish to compute the $j+1$ subspace that is far from all the previous subspaces on the Grassmannian. The squared chordal distance between $\mathbf{V}_{j+1}$ and $\mathbf{V}_{i}$, where $i\leq j$ is given by
\begin{align}
\nonumber
d^2(\mathbf{V}_{j+1},\mathbf{V}_{i}) &= \max(N_{j+1},N_i)-\|\mathbf{V}_i^T\mathbf{V}_{j+1}\|_F^2,\\
\nonumber
&= \max(N_{j+1},N_i)-\text{Tr}\left(\mathbf{V}_{j+1}^T \mathbf{V}_i \mathbf{V}_i^T \mathbf{V}_{j+1}\right),
%\label{eqn:dist_j1_i}
\end{align} and hence the sum of squared distances $\sum_{i=1}^j d^2(\mathbf{V}_{j+1},\mathbf{V}_{i})$ is given as
\begin{align}
\sum_{i=1}^j \max(N_{j+1},N_i)-\text{Tr}\left(\mathbf{V}_{j+1}^T \sum_{i=1}^j \left( \mathbf{V}_i \mathbf{V}_i^T \right) \mathbf{V}_{j+1}\right).
\label{eqn:dist_j1_i2}
\end{align} Maximizing (\ref{eqn:dist_j1_i2}) w.r.t. $\mathbf{V}_{j+1}$ is equivalent to minimizing the negative of the second term. Combining this with (\ref{eqn:graphemb_LE}), the overall optimization to compute $\mathbf{V}_{j+1}$ is given as
\begin{align}
\mathbf{V}_{j+1} = \argmin_{\mathbf{V}^T \mathbf{X} \mathbf{H} \mathbf{X}^T \mathbf{V} = \mathbf{I}} \text{Tr}\left(\mathbf{V}^T \left( \mathbf{X} \mathbf{L} \mathbf{X}^T+ \alpha \sum_{i=1}^j \left( \mathbf{V}_i \mathbf{V}_i^T \right) \right) \mathbf{V}\right),
\label{eqn:graphemb_LE_incoh}
\end{align} where $\alpha > 0$ is the tradeoff parameter between the embedding cost and the incoherence of the subspace. This can be solved by choosing $N_{j+1}$ eigen vectors corresponding to the smallest eigen values from the GED
\begin{align}
\left( \mathbf{X} \mathbf{L} \mathbf{X}^T + \alpha \sum_{i=1}^j \left( \mathbf{V}_i \mathbf{V}_i^T \right) \right) \mathbf{v} = \lambda \mathbf{X} \mathbf{H} \mathbf{X}^T \mathbf{v}.
\label{eqn:graphemb_LE_GEVD_1}
\end{align} The $K$ incoherent subspaces are obtained by repeating this process $K$ times, by including an additional subspace in each round. Note that we orthonormalize the columns of subspace basis computed in each round before proceeding to the next round.

\subsection{Grassmannian Averaging}
\label{sec:grass_avg}
After obtaining the $K$ subspaces, $\{V_i\}_{i=1}^K$, we will compute the consensus (mean) subspace that will have minimum sum of squared chordal distance with these subspaces. The dimension of the mean subspace spanned by the columns of $\mathbf{V}$, will be chosen to be the maximum of $\{N_i\}_{i=1}^K$ and we will denote it as $N_m$. The optimization problem can be now posed as
\begin{align}
\nonumber
\mathbf{V} &= \argmin_{\mathbf{V}^T\mathbf{V} = \mathbf{I}} \sum_{i=1}^K w_i \left(N_m-\|\mathbf{V}^T\mathbf{V}_i\|_F^2\right),\\
&= \argmin_{\mathbf{V}^T\mathbf{V}= \mathbf{I}} \text{Tr} \left( \mathbf{V}^T \sum_{i=1}^K \left(w_i \mathbf{I} - w_i \mathbf{V}_i \mathbf{V}_i^T \right) \mathbf{V} \right) 
\label{eqn:grass_avg}
\end{align} where $w_k$ are the optional positive weights. $\mathbf{V}$ can be evaluated as the $N_m$ eigen vectors corresponding to the smallest eigen values of $\sum_{i=1}^K \left(w_i \mathbf{I} - w_i \mathbf{V}_i \mathbf{V}_i^T \right)$.

\subsection{Supervised Embedding with Trace Ratio}
\label{sec:sup_emb_tr} 
As discussed in Section \ref{sec:graphEmb}, globally optimal projection directions can be obtained using trace ratio optimization in supervised embedding. This can be exploited in our proposed approach as well for supervised embedding. The only modification will be in obtaining the incoherent subspaces, $\{V_i\}_{i=1}^K$. We will incorporate the incoherence constraint of (\ref{eqn:dist_j1_i2}) into (\ref{eqn:trratio}) and obtain the following optimization,
\begin{eqnarray}
\mathbf{V}_{j+1} = \argmax_{\mathbf{V}^T \mathbf{V} = \mathbf{I}} \frac{\text{Tr}\left(\mathbf{V}^T \mathbf{X} \mathbf{H} \mathbf{X}^T \mathbf{V}\right)} {\text{Tr}\left(\mathbf{V}^T \left( \mathbf{X} \mathbf{L} \mathbf{X}^T + \alpha \sum_{i=1}^j \left( \mathbf{V}_i \mathbf{V}_i^T \right)\right) \mathbf{V}\right)}.
\label{eqn:trratio_incoh}
\end{eqnarray} This can be solved using the existing trace ratio optimization approaches such as DNM or ITR. After computing the incoherent subspaces, they can be averaged using the approach described in Section \ref{sec:grass_avg} to obtain the final subspace.

\begin{figure*}[ht]
\begin{minipage}[b]{0.32\linewidth}
 \centering
 \centerline{(a) Landsat dataset.}
\includegraphics[width = 6cm]{landsat_d10.png}
\end{minipage}
\hfill
\begin{minipage}[b]{0.32\linewidth}
 \centering
 \centerline{(b) Extended YaleB dataset.}
\includegraphics[width = 5.8cm]{yale_d30.png}
\end{minipage}
\hfill
\begin{minipage}[b]{0.32\linewidth}
 \centering
 \centerline{(c) USPS Digits dataset.}
\includegraphics[width = 6cm]{usps_d10.png}
\end{minipage}
\caption{Supervised Graph Embedding: Mean precision - Mean Recall curves for novel test data with respect to the training samples.}
\label{Fig:sup}
\end{figure*}

\section{Results and Discussion}
\label{sec:expt}
We evaluate the proposed algorithms using unsupervised and supervised, linearized graph embedding algorithms on different standard datasets. A natural way to evaluate the usefulness of dimensionality reduction algorithms is to  measure the quality of the preserved neighborhood with respect to the original high-dimensional data. From an information retrieval perspective, this is equivalent to measuring the precision/recall of the relevant neighbors. A high-quality embedding can significantly impact the interpretability and visualization of high-dimensional data. In addition, for the case of supervised embeddings, we measure the classification performance using a simple $k$-Nearest Neighbor ($k$-NN) classifier.

\subsection{Unsupervised Graph Embedding}
For unsupervised learning experiments, we consider Locality Preserving Projections (LPP), Neighborhood Preserving Embedding (NPE), and the proposed variants of these approaches. To infer the LPP projections, we construct a $k$-NN graph and compute similarities using a heat kernel, $W_{ij} = \exp(-\gamma  \|\mathbf{x}_i - \mathbf{x}_j\|_2)$. For NPE, the similarity graph is constructed using a least-squares fit in the neighborhood of each sample. In both approaches, we fixed the neighborhood size, $k = 15$. For the proposed algorithm, we choose the size of the ensemble $K = 20$ and learn multiple incoherent subspaces with LPP and NPE respectively. 

We measure the quality of the embedding by choosing $N_h = 50$ neighbors for each sample in the high-dimensional space and varying the neighborhood size $N_l$ between $1$ and $200$ in the low-dimensional space. For each sample index $i$, we measure the precision and recall in the embedding as
\begin{equation}
precision(i) = \frac{|\Omega_h(i) \cap \Omega_l(i)|}{N_l},
\end{equation}and
\begin{equation}
recall(i) = \frac{|\Omega_h(i) \cap \Omega_l(i)|}{N_h},
\end{equation}where $\Omega_h$ and $\Omega_l$ denote the set of neighborhood samples for original and embedded data respectively. Finally, the set of precision-curves are generated by averaging across all data samples. 

We used the following datasets for our experiments: (a) faces dataset \cite{roweis} that contains $10$ different face images of $40$ different subjects; (b) sea-water temperature time-series dataset \cite{seawater}, where each data sample is a time window of $52$ weeks; (c) UCI ecoli dataset of protein localization sites \cite{uci}; (d) face videos dataset \cite{face}. In each case, we repeated experiments with varying number of embedding dimensions ($N$) and report the results for the case which provided the highest F-measure ($2(P.R)/(P + R)$).

\begin{table}[tb] 
  \setlength{\tabcolsep}{8pt}
  \renewcommand*{\arraystretch}{1}
  \centering
  \caption{Classification Performance obtained using different supervised embedding techniques. In each case, the performance was evaluated using a k-NN classifier and the highest performance for each dataset is shown in bold. GM denotes our proposed approach.}
  \vspace{0.1 in}
    \begin{tabular}{|c|c|c|c|c|c|}
   \hline
    \textbf{Dataset}& \textbf{LDA} &  \textbf{LDE} & \textbf{LDA-GM} & \textbf{LDE-GM} \\
    \hline
    \hline
    \small
    \textbf{Landsat}&83.99&84.2&89.9&\textbf{91.45}\\
\textbf{Letter}&88.95&91.43&89.33&\textbf{92.25}\\
\textbf{USPS}&92.79&93.51&89.17&\textbf{96.7}\\
\textbf{HeartDisease}&69.7&82.5&77.5&\textbf{84.9}\\
\textbf{YaleB}&78.55&85.64&89.67&\textbf{91.57}\\
\textbf{ORL}&93.33&92.9&95.1&\textbf{95.7}\\
	\hline
    \end{tabular}%
  \label{Table:sup}%
\end{table}

Figure \ref{Fig:unsup} shows the mean precision-mean recall curves for the four datasets obtained using different linear graph embedding strategies. As it can be clearly observed, the proposed algorithm significantly improves the performance of both LPP and NPE approaches. It is particularly interesting to note that it does not compromise the recall performance in order to better preserve the closest neighbors. In order to illustrate the utility of the proposed algorithm in data visualization, we consider the UCI ecoli dataset and show the $2-$D embeddings obtained using NPE and the proposed algorithm in Figure \ref{Fig:unsup}(e) and (f) respectively. Though the embedding is constructed using unsupervised strategies, the relation between the different classes (color) is strongly evident in the proposed embedding and hence contains much richer information from a visualization perspective.

\subsection{Supervised Graph Embedding}
In the next set of experiments, two linearized graph embedding algorithms (Linear Discriminant Analysis (LDA) and Local Discriminant Embedding (LDE)) are used with the proposed technique to infer a consensus subspace for supervised classification. In both cases, the projection directions are computed using the iterative trace ratio (ITR) method. The inter-class and intra-class neighborhood sizes for learning the LDE projections were fixed at $10$. Similar to the previous experiment, we used $K = 20$ subspaces to infer the consensus. In order to evaluate the performance of the embeddings, we split each dataset into train and test sets ($70\%$ for training), learned the subspace using the train set, and projected the test samples onto the inferred subspace. Similar to an information retrieval setup, we measure the precision and recall for each test sample with respect to the training samples, in the embedded space (using ground truth labels). In addition, we measure the classification accuracy using a $k$-NN classifier ($k=5$).

Table \ref{Table:sup} lists the classification performance of the different supervised embedding strategies with the following datasets: (a) UCI landsat dataset \cite{uci}; (b) UCI letter recognition dataset \cite{uci}; (c) USPS handwritten digits \cite{uci}; (d) UCI heart disease dataset \cite{uci}; (e) Extended YaleB dataset \cite{yale}; (f) ORL face database \cite{roweis}. Figure \ref{Fig:sup} illustrates the mean precision-mean recall curves for the test data with different embedding strategies. Significant performance gains were obtained using the proposed algorithm in all cases. 

\section{Conclusions}
In this paper, we proposed to build consensus of multiple incoherent subspace projections on the Grassmannian, to obtain high-quality graph embeddings. Evaluation with popular unsupervised and supervised approaches show lot of promise, and we believe it is crucial to study the theoretical characteristics of the proposed approach. In addition, kernelization of the proposed algorithm can further improve its performance on more complex datasets.



\bibliographystyle{IEEE}
\bibliography{grassmannOpt}

\end{document}


