function y = gradientTest(fcn, x1, pert)

% input: 
    % function handle fcn: R^n -> R with syntax [f g] = fcn(x, para).
    % initial point x1
    % para: auxillary parameters that may be needed by fcn
    % pert: maximum element-wise perturbation, e.g. 1e-3. 

 % output: 
    % y is the deviation, as explained in gradientTest.pdf, between 1 and
    % the quantity that should be close to 1. 
    
    
sizeVar = size(x1);

%N = para.N;
%K = para.K; 
%T = para.T; 
T = 1; 
% Real perturbation: 
eps = pert*randn(sizeVar);% + 1i*pert*randn(n,1);
%eps(1:N*K) = 0;
%eps(N*K+1:end) = 0;


% General complex perturbation: 
%eps = pert*randn(n, 1) + 1i*pert*randn(n,1);

const = 10;
eps = eps*const^2;
fprintf('\nRunning gradient test:\n\n');

[f1 g1] = fcn(x1);
    
    
fprintf('Change, result\n');
for i = 1:5
    eps = eps/const;
    x2 = x1 + eps;
    
    [f2 g2] = fcn(x2);
    
    %inds = find(f1 > 0);
    %indTest = inds(1);
    %testTemp = g1(indTest, :) + g2(indTest, :);
    
    % This must be close to 1 (based on finite diff. defn. of gradient)
    temp = 0.5 *(g1(:) + g2(:))'*(eps(:))./(f2 - f1);
    
    % Hence this must be close to 0
    y = max(abs(1-real(temp)));
    fprintf('%4.2E, %4.2E\n', sum(sum(sum(eps.*eps))), y);
    
end
fprintf('\n\n');
end