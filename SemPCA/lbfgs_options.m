% Add path for the solver

% Solver Options
options.wolfe1   = 1e-4;            % line search constant 1
options.wolfe2   = 0.1;             % line search constant 2
options.maxit    = 500;             % maximum number of iterations
options.gnormtol = 1e-6; %1e-16;           % stops if norm(g) < gnormtol
options.stpsztol = 1e-16;           % stops if step size < stpsztol
options.prtlvl   = 2;               % printlvl: 0, 1 or 2
options.gathdata = 0;               % rundata gathering: 0 or 1
options.printto  = 1;               % print to screen (1) or file ('filename')
options.m        = 9;               % number of LBFGS updates stored