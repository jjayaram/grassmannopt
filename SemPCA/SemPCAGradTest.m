% Test gradient for the computation of the node probabilities (Pi)
d = 2;
N = 100;
M= 5;

W = rand(N,N);
W = W+W';

Y = randn(d,N,M);
D = zeros(N,N,M);
Tau = rand(N,M);
Pi = exp(-Tau);
Pi = bsxfun(@times,Pi,1./sum(Pi,2));

% Distance computation
for m = 1:M
    D(:,:,m) = L2_distance(Y(:,:,m),Y(:,:,m)).^2;
end

% Gradient code with Pi
m = 2;
[f1,g1] = SemPCACostGrad_Pi(D,W,Pi,m);
eps_arr = 1e-3*rand(N,1);
Tau(:,m) = Tau(:,m)+eps_arr;
Pi = exp(-Tau);
Pi = bsxfun(@times,Pi,1./sum(Pi,2));
[f2,g2] = SemPCACostGrad_Pi(D,W,Pi,m);

temp = 0.5 *(g1(:) + g2(:))'*(eps_arr(:))./(f2 - f1);
y = max(abs(1-real(temp)))

% Gradient code with Tau
pars.D      = D;
pars.W      = W;
pars.Tau    = Tau;
pars.m      = m;
y = gradientTest(@(x)SemPCACostGrad_Tau(x,pars),Tau(:,m),1e-4)