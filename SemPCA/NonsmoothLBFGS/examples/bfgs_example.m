clc; clear;                         % clear the screen and memory
                                    %
options.wolfe1   = 1e-4;            % line search constant 1
options.wolfe2   = 0.1;             % line search constant 2
options.maxit    = 500;             % maximum number of iterations
options.gnormtol = 1e-16;           % stops if norm(g) < gnormtol
options.fvalquit = 1e-12;           % stops if f < fvalquit
options.stpsztol = 1e-16;           % stops if step size < stpsztol
options.foptval  = 0;               % the REAL optimal value (optional)
options.prtlvl   = 2;               % printlvl: 0, 1 or 2
options.gathdata = 0;               % rundata gathering: 0 or 1
options.printto  = 1;               % print to screen (1) or file ('filename')
                                    %   
pars.fgname = 'tiltednormcond';     % name of objective function (string)
pars.p      = 2;                    % parameter parsed to obj. function
pars.w      = 8;                    % parameter parsed to obj. function
pars.nvar   = 10;                   % number of variables
pars.A      = rand(pars.nvar);      % parameter parsed to obj. function
                                    %
options.x0  = randn(pars.nvar,1);   % initial x. here random.
                                    %
[x,f,rd] = bfgs(pars,options);      % run the algorithm
                                    %
% x is the minizer found
% f is the optimal value found
% rd is a struct containing data about the run
