function [f,g] = chained_LQ(x,pars)

% printthis:
% Chained LQ (convex func):
% f = sum_i( max( -x_i-x_{i+1},-x_i-x_{i+1}+x_i^2+x_{i+1}^2-1 ) )
% 0 parameters: w, w
% show: n, n

%n = pars.nvar;
n = length(x);
f = 0;
g = zeros(n,1);

for i = 1:(n-1)
    a = -x(i)-x(i+1);
    b = a + ( x(i)^2 + x(i+1)^2 - 1 );
    if a >= b
        f = f + a;
        g(i)   = g(i) - 1;
        g(i+1) = -1;
    else
        f = f + b;
        g(i)   = g(i) - 1 + 2*x(i);
        g(i+1) = -1 + 2*x(i+1);
    end
end