function [pars,ops] = getprob_f10(pn,pars,ops)

switch pn
    case 1  % gen_maxq
        pars.fgname   = 'gen_maxq';
        v          = 1:pars.nvar;
        n1         = floor(pars.nvar/2);
        ops.x0(1:n1) = v(1:n1);
        ops.x0((n1+1):pars.nvar) = -v((n1+1):pars.nvar);
        ops.foptval  = 0;

    case 2  % gen_maxhilb
        pars.fgname = 'gen_maxhilb';
        ops.x0      = ones(pars.nvar,1);
        ops.foptval = 0;        

    case 3  % chained_LQ
        pars.fgname = 'chained_LQ';
        ops.x0      = -0.5*ones(pars.nvar,1);
        ops.foptval = -(pars.nvar-1)*sqrt(2);        

    case 4  % chained_CB3v1
        pars.fgname = 'chained_CB3v1';
        ops.x0      = 2*ones(pars.nvar,1);
        ops.foptval = 2*(pars.nvar-1);        

    case 5  % chained_CB3v2
        pars.fgname = 'chained_CB3v2';
        ops.x0      = 2*ones(pars.nvar,1);
        ops.foptval = 2*(pars.nvar-1);                

    case 6  % nactfaces
        pars.fgname = 'nactfaces';
        ops.x0      = ones(pars.nvar,1);
        ops.foptval = 0;                

    case 7  % gen_brownfunc2
        pars.fgname = 'gen_brownfunc2';
        ops.x0      = ones(pars.nvar,1);
        ops.x0(1:2:pars.nvar) = -1;
        ops.foptval  = 0;                

    case 8  % chained_mifflin2
        pars.fgname = 'chained_mifflin2';
        ops.x0     = -ones(pars.nvar,1);
        switch pars.nvar
            case 10
                ops.foptval = -6.509095301609023;
            case 50
                ops.foptval = -34.769917185531163;
            case 200
                ops.foptval = -1.408472051845424e+02;                
            case 1000
                ops.foptval = -7.065033536440673e+02;  
            case 2000
                ops.foptval = -1.413593736481757e+03;
            case 5000
                ops.foptval = -3.534821614306861e+03;
            case 10000
                ops.foptval = -7.070496537268926e+03;
            otherwise
                fprintf('WARNING: Setting foptval = -inf \n');
                ops.foptval = -inf;
        end

    case 9  % chained_crescent1
        pars.fgname = 'chained_crescent1';
        ops.x0     = 2*ones(pars.nvar,1);
        ops.x0(1:2:pars.nvar) = -1.5;
        ops.foptval  = 0;                

    case 10 % chained_crescent2
        pars.fgname = 'chained_crescent2';
        ops.x0     = 2*ones(pars.nvar,1);
        ops.x0(1:2:pars.nvar) = -1.5;
        ops.foptval  = 0;                


end
ops.x0 = ops.x0(:);
