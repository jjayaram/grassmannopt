function [f,g] = gen_brownfunc2(x,pars)

% printthis:
% Non-smooth generalization of brown function 2 (non-convex, non-smooth):
% f = sum_i ( abs(x_i)^{x_{i+1}^2+1} + abs(x_{i+1})^{x_i^2+1} )
% 0 parameters: w, w
% show: n, n

%n = pars.nvar;
n = length(x);
f = 0;
g = zeros(n,1);

for i = 1:n-1
    a = abs(x(i));
    b = abs(x(i+1));
    c = x(i)^2+1;
    d = x(i+1)^2+1;
    f = f + b^c + a^d;
    
    p = 0;
    q = 0;
    if x(i) < 0
        if b > p
            p = log(b);
        end
        g(i) = g(i) - d*a^(d-1)+2*x(i)*p*b^c;
    else
        if b > p
            p = log(b);
        end
        g(i) = g(i) + d*a^(d-1)+2*x(i)*p*b^c;
    end
    if x(i+1) == 0
        g(i+1) = 0;
    elseif x(i+1) < 0
        if a > q
            q = log(a);
        end
        g(i+1) = -c*b^(c-1)+2*x(i+1)*q*a^d;
    else
        if a > q
            q = log(a);
        end
        g(i+1) = c*b^(c-1)+2*x(i+1)*q*a^d;
    end
        
end
