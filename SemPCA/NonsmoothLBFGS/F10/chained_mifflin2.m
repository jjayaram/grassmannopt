function [f,g] = chained_mifflin2(x,pars)

% printthis:
% Chained Mifflin II (non-convex, non-smooth func):
% f = 
% 0 parameters: w, w
% show: n, n

%n = pars.nvar;
n = length(x);
f = 0;
g = zeros(n,1);

for i = 1:(n-1)
    
    y = x(i)^2 + x(i+1)^2 - 1;
    f = f - x(i) + 2*y + 1.75*abs(y);
    y = fsign(3.5,y) + 4;
    g(i) = g(i) + y*x(i) - 1;
    g(i+1) = y*x(i+1);
    
end