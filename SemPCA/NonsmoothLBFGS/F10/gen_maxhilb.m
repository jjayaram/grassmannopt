function [f,g] = gen_maxhilb(x,pars)

% printthis:
% Max of Hilberts (convex func):
% f = max_i sum_j abs( x_j / (i+j-1) )
% 0 parameters: w, w
% show: n, n

%n  = pars.nvar;
n = length(x);
v  = (1:n)';
vm = repmat(v,1,n);
xm = repmat(x',n,1);

M     = sum(xm./(vm + vm' - 1),2);
[f,s] = max(abs(M));
sa    = sign(M(s));
g     = sa./(s+v-1);

%  %FORTRAN
%    
% f = 0;    
% k = 1;    
% n = pars.nvar;
% g = zeros(n,1);
% 
% 
% for j = 1:n
%     f = f + x(j)/j;
% end
% g(1) = sign(f);
% f    = abs(f);
% for i = 2:n
%     t2 = 0;
%     for j = 1:n
%         t2 = t2 + x(j)/(i+j-1);
%     end
%     g(i) = sign(t2);
%     t2 = abs(t2);
%     if t2 > f
%         f = t2;
%         k = i;
%     end
% end
% t3 = g(k);
% for j = 1:n
%     g(j) = t3/(k+j-1);
% end



