function [f,g] = gen_maxq(x,pars)

% printthis:
% Max of quadratics (convex func):
% f = max (x_i)^2, i = 1,..,n
% 0 parameters: w, w
% show: n, n

%n  = pars.nvar;
n = length(x);

g   = zeros(n,1);
v       = x.^2;
[m,idx] = max(v);
f       = m;
g(idx)  = 2*x(idx);

%FORTRAN
%f   = x(1)^2;
%k   = 1;
%n   = pars.nvar;
%for j = 2:n
%    y = x(j)^2;
%    if y > f
%        f = y;
%        k = j;
%    end
%end
%g(k) = 2*x(k);
