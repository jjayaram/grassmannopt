function [f,g] = nactfaces(x,pars)

% printthis:
% Number of active faces (non-convex, non-smooth func):
% f = max_i {g(x_i),g(-sum_j x_j)}, g(y) = ln(abs(y)+1)
% 0 parameters: w, w
% show: n, n

%n = pars.nvar;
n = length(x);
f = 0;
g = zeros(n,1);
k = 1;

t3 = 1;
y  = -x(1);
f  = log( abs(x(1)) + 1);
t2 = f;

for i = 2:n
    
    y    = y - x(i);
    g(i) = 0;
    f    = max(f,log(abs(x(i))+1) );
    if f > t2
        k  = i;
        t2 = f;
    end
end
f = max(f,log(abs(y)+1));
if f > t2
    if y > 0
        t3 = -1;
    end
    for i = 1:n
        g(i) = t3*( 1 / (abs(y)+1) );
    end
else
    if x(k) < 0
        t3 = -1;
    end
    g(k) = t3*( 1/ (abs(x(k))+1 ));
end