function [f,g] = chained_crescent1(x,pars)

% printthis:
% Chained Crescent 1:
% f = 
% 0 parameters: w, w
% show: n, n

%n = pars.nvar;
n = length(x);
f = 0;
g = zeros(n,1);

t2 = 0;
t3 = 0;

for i = 1:n-1 
    
    t2 = t2 + x(i)^2 + (x(i+1)-1)^2 + x(i+1) - 1;
    t3 = t3 - x(i)^2 - (x(i+1)-1)^2 + x(i+1) + 1;
    
end
f = max(t2,t3);
if t2 >= t3
    for i = 1:n-1
        g(i) = g(i) + 2*x(i);
        g(i+1) = 2*(x(i+1)-1) + 1;
    end
else
    for i = 1:n-1
        g(i) = g(i) - 2*x(i);
        g(i+1) = -2*(x(i+1)-1) + 1;
    end
end

      
