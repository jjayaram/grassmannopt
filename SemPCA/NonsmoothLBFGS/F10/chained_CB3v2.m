function [f,g] = chained_CB3v2(x,pars)

% printthis:
% Chained CB3 II (convex, non-smooth func):
% f = 
% 0 parameters: w, w
% show: n, n

%n = pars.nvar;
n = length(x);
f = 0;
g = zeros(n,1);
a = 0; b = 0; c = 0;

for i = 1:n-1
    
    a = a + x(i)^4 + x(i+1)^2;
    b = b + (2-x(i))^2 + (2-x(i+1))^2;
    c = c + 2*exp(-x(i)+x(i+1));
    
end
f = max(a,b);
f = max(f,c);
if f == a
    for i = 1:n-1
        g(i)   = g(i) + 4*x(i)^3;
        g(i+1) = 2*x(i+1);
    end
elseif f == b
    for i = 1:n-1
        g(i) = g(i) + 2*x(i) - 4;
        g(i+1) = 2*x(i+1) - 4;
    end
else
    for i = 1:n-1
        g(i) = g(i) - 2*exp(x(i)+x(i+1));
        g(i+1) = 2*exp(-x(i)+x(i+1));
    end
end





% 50   CONTINUE
%       F=0.0D+00
%       G(1)=0.0D+00
%       A=0.0D+00
%       B=0.0D+00
%       C=0.0D+00
% 
%       DO 51 I=1,N-1
%          G(I+1)=0.0D+00
%          A=A+X(I)*X(I)*X(I)*X(I)+X(I+1)*X(I+1)
%          B=B+(2.0D+00-X(I))*(2.0D+00-X(I))+
%      &        (2.0D+00-X(I+1))*(2.0D+00-X(I+1))
%          C=C+2.0D+00*DEXP(-X(I)+X(I+1))
%  51   CONTINUE
%       F=DMAX1(A,B)
%       F=DMAX1(F,C)
%       IF (F .EQ. A) THEN
%          DO 53 I=1,N-1
%             G(I)=G(I)+4.0D+00*X(I)*X(I)*X(I)
%             G(I+1)=2.0D+00*X(I+1)
%  53      CONTINUE
%       ELSE IF (F .EQ. B) THEN
%          DO 54 I=1,N-1
%             G(I)=G(I)+2.0D+00*X(I)-4.0D+00
%             G(I+1)=2.0D+00*X(I+1)-4.0D+00
%  54      CONTINUE
%       ELSE
%          DO 55 I=1,N-1
%             G(I)= G(I) - 2.0D+00*DEXP(-X(I)+X(I+1))
%             G(I+1)= 2.0D+00*DEXP(-X(I)+X(I+1))
%  55      CONTINUE
%       END IF
% 
%       RETURN
