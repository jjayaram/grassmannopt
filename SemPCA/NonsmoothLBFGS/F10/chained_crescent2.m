function [f,g] = chained_crescent2(x,pars)

% printthis:
% Chained Crescent 2:
% f = 
% 0 parameters: w, w
% show: n, n

%n = pars.nvar;
n = length(x);
f = 0;
g = zeros(n,1);

for i = 1:n-1 
    t2 = x(i)^2 + (x(i+1)-1)^2 + x(i+1) - 1;
    t3 = -x(i)^2 - (x(i+1)-1)^2 + x(i+1) + 1;
    if t2 >= t3
        f = f + t2;
        g(i)   = g(i) + 2*x(i);
        g(i+1) = 2*(x(i+1)-1) + 1;
    else
        f = f + t3;
        g(i)   = g(i) - 2*x(i);
        g(i+1) = -2*(x(i+1)-1) + 1;
    end
end
     