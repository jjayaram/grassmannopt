function [f,g] = chained_CB3v1(x,pars)

% printthis:
% Chained CB3 I (convex, non-smooth func):
% f = 
% 0 parameters: w, w
% show: n, n

%n = pars.nvar;
n = length(x);

f = 0;
g = zeros(n,1);

for i = 1:n-1
    
    a = x(i)^4 + x(i+1)^2;
    b = (2-x(i))^2 + (2-x(i+1))^2;
    c = 2*exp(-x(i)+x(i+1));
    y = max(a,b);
    y = max(y,c);
    if y == a
        g(i)   = g(i) + 4*x(i)^3;
        g(i+1) = 2*x(i+1);
    elseif y == b
        g(i)   = g(i) + 2*x(i) - 4;
        g(i+1) = 2*x(i+1) - 4;
    else
        g(i)   = g(i) - c;
        g(i+1) = c;
    end
    f = f+y;
end

         
         
         