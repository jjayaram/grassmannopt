function ops = setwolfedefaults(ops, CG)
%  check Wolfe line search fields for ops and set defaults
%  CG is 1 for CG methods, 0 for BFGS methods
if isfield(ops, 'strongwolfe')
    if ops.strongwolfe ~= 0 & ops.strongwolfe ~= 1
        error('setwolfedefaults: input "ops.strongwolfe" must be 0 or 1')
    end
else
    if CG
        ops.strongwolfe = 1;  % needed for convergence analysis
    else
        ops.strongwolfe = 0;  % not needed for convergence analysis
        % strong Wolfe is very complicated and is bad for nonsmooth functions
    end
end
if isfield(ops, 'wolfe1') 
    if ~isposreal(ops.wolfe1)
        error('setwolfedefaults: input "ops.wolfe1" must be a positive real scalar')
    end
else
    ops.wolfe1 = 1e-4;
end
if isfield(ops, 'wolfe2')
    if ~isposreal(ops.wolfe2)
        error('setwolfedefaults: input "ops.wolfe2" must be a positive real scalar')
    end
elseif CG == 1
    ops.wolfe2 = 0.49;  % must be < .5 for CG convergence theory
else
    ops.wolfe2 = 0.9;   % must be < 1 for BFGS update to be pos def
end
if ops.wolfe1 <= 0 | ops.wolfe1 >= ops.wolfe2 | ops.wolfe2 >= 1
    fprintf('setwolfedefaults: Wolfe line search parameters violate usual requirements')
end
if ops.prtlvl > 0
    if ops.strongwolfe & ~CG
        fprintf('Strong Wolfe line search selected, but for BFGS or LMBFGS\n')
        fprintf('weak Wolfe may be preferable, especially if f is nonsmooth\n')
    elseif ~ops.strongwolfe & CG 
        fprintf('Weak Wolfe line search selected, but for CG\n')
        fprintf('this often fails: use strong Wolfe instead')
    end
end
            