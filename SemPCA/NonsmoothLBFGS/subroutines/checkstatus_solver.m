function [curstate,prtstr] = checkstatus_solver(rd,ops,pars)

curstate = 0; prtstr = '';

if rd.fs(rd.k) < ops.fvalquit
    curstate = 2;
    prtstr   = 'f < fvalquit.';
end

if isfield(rd,'lsfail') && rd.lsfail == 1 % Wolfe conditions not both satisfied, quit
    curstate = -1;
    prtstr   = 'Wolfe conds not both satisfied.';    
elseif isfield(rd,'lsfail') && rd.lsfail == -1 % function apparently unbounded below
    curstate = -1;
    prtstr   = 'f may be unbounded below.';
end

if rd.gnorms(rd.k) <= ops.gnormtol
    curstate = 1;
    prtstr   = 'norm(g) < gnormtol.';
end

if ~strcmp(pars.solver,'redistprox') && rd.stpszs(rd.k) < ops.stpsztol
    curstate = 1;
    prtstr   = 'stepsize < stpsztol.';
end

if rd.k == ops.maxit
    curstate = -1;
    prtstr   = 'max iters reached.';
end

if  curstate ~= 0
    prtstr = ['stopped b/c: ',prtstr];
end
