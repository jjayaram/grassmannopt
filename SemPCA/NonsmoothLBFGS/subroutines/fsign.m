function s = fsign(x,y)

% the transfer sign function as defined in FORTRAN:
% The sign transfer function SIGN(X,Y)  takes the sign of the second
% argument and puts it on the first argument, i.e.:
% ABS(X)  if Y >= 0  and -ABS(X)  if Y < 0

if y >= 0
    s = abs(x);
else
    s = -abs(x);
end