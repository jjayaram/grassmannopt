function initialchecks_solver(f,g,gnorm,ops)


if size(g,2) > size(g,1) % error return is appropriate here
    error('gradient must be returned as a column vector, not a row vector')
end

if f == inf % better not to generate an error return
    if ops.prtlvl > 0
        fprintf('solver: f is infinite at initial iterate\n')
    end
    return
elseif isnan(f)
    if ops.prtlvl > 0
        fprintf('solver: f is nan at initial iterate\n')
    end
    return
elseif gnorm < ops.gnormtol
    if ops.prtlvl > 0
        fprintf('solver: tolerance on gradient satisfied at initial iterate\n')
    end
    return
elseif f < ops.fvalquit
    if ops.prtlvl > 0
        fprintf('solver: below target objective at initial iterate\n')
    end
    return
end