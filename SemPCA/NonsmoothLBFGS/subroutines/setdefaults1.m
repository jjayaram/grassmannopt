function options = setdefaults1(pars,options)
%  call: options = setdefaults(pars,options)
%  check that fields of pars and options are set correctly and
%  set basic default values for options that are common to various 
%  optimization methods, including bfgs, cgprfr and gradsamp
if nargin < 2
    options = [];
end
% if ~isfield(pars, 'nvar')
%    error('setdefaults: input "pars" must have a field "nvar" (number of variables)')
% elseif ~isposint(pars.nvar)
%    error('setdefaults: input "pars.nvar" (number of variables) must be a positive integer')
% end
% if ~isfield(pars, 'fgname')
%    error('setdefaults: input "pars" must have a field "fgname" (name of m-file computing function and gradient)')
% end
if isfield(options, 'maxit')
    if ~isnonnegint(options.maxit)
        error('setdefaults: input "options.maxit" must be a nonnegative integer')
    end
else
    options.maxit = 10000;
end
if isfield(options, 'gnormtol')
    if ~isposreal(options.gnormtol)
        error('setdefaults: input "options.gnormtol" must be a positive real scalar')
    end
else
    options.gnormtol = 1.0e-8;
end
if isfield(options, 'fvalquit')
    if ~isreal(options.fvalquit)|~isscalar(options.fvalquit)
        error('setdefaults: input "options.fvalquit" must be a real scalar')
    end
else
    options.fvalquit = -inf;
end

if ~isfield(options,'stpsztol')
    options.stpsztol = 0;
end

