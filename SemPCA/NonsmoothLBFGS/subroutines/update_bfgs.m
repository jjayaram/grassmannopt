function [p,H] = update_bfgs(pprev,alpha,gkp1,g,k,H)

s       = alpha*pprev;
y       = gkp1 - g;
sty     = s'*y;
if k == 1   % scale only after first iteration
   H = (sty/(y'*y))*H; 
end
rho     = 1/sty;
rhoHyst = rho*(H*y)*s';
H       = H - rhoHyst' - rhoHyst + rho*s*(y'*rhoHyst) + rho*s*s';
p       = -H*gkp1;  % next search dir