function inni = isnonnegint(x)
% return true if x is a nonnegative integer, false otherwise 
if ~isscalar(x) 
    inni = 0;
else % following is OK since x is scalar
    inni = (isreal(x) & round(x) == x & x >= 0);
end
   