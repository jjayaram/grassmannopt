function [curstate,prtstr] = checkfordescentdir_solver(gtp)

curstate = 0; prtstr = '';
if gtp >= 0 || isnan(gtp) % in rare cases, H could contain nans
    curstate = 1;
    prtstr   = 'not descent direction';
end

if  curstate ~= 0
    prtstr = ['stopped b/c: ',prtstr];
end