function [x, f, rundata] = solverwrap(pars, ops)

if nargin == 0
    error('"pars" is a required input parameter')
end
if nargin == 1
    ops = [];
end

ops = setdefaults2(pars,ops);     % set default ops
ops = setwolfedefaults(ops, 0);   % fields for Wolfe line search

if ops.printto == 1
    fid = 1;
    ops.dg = 2;
else
    fid = fopen(ops.printto,'a');
    ops.dg = 5;
end
ops.fid = fid;

slv = getsolvdat(pars.solver);


% -------------- printing: -------------------
printfunc(10,ops,pars,[]); % Originally 1 -- NRK
% ------------- end printing ------------------

% ACTUAL CALL TO SOLVER: ----------------------
[x, f, rundata] = feval(slv.runfunc,pars,ops);
% ----------------------------------------------


if ops.gathdata == 1
    rundata.mnmzr   = x;
else
    if pars.nvar < 1e4
        rundata.mnmzr = x;
    end
end

rundata.ffinal = f;

if isfield(ops,'foptval')
    rundata.foptval = ops.foptval;
end

rundata.ops     = ops;
rundata.pars    = pars;


% -------------- printing: -------------------
if ops.prtlvl > 0
    fprintf(fid,'---------------------------------');
    fprintf(fid,'---------------------------------\n');
    fprintf(fid,[rundata.prtstr,'\n',...
        'time spent:',spcs(1),' %1.3e sec.\n'],...
        rundata.ttime);
    fprintf(fid,['# fun.eval:',spcs(1),' %i \n'],rundata.nfeval);
    fprintf(fid,['target fval:',spcs(0),' %1.16e \n'],rundata.ops.fvalquit);
    fprintf(fid,['final fval:',spcs(1),' %1.16e \n'],rundata.ffinal);
    fprintf(fid,'==============================END');
    fprintf(fid,'=================================\n');
end
% ------------- end printing ------------------


% ------------------------------------------

if ops.gathdata ~= 1 && pars.nvar >= 1e3

    rundata.ops.x0 = rundata.ops.x0(1:20);

else

    rundata.ops.x0 = rundata.ops.x0;

end

if ops.printto == 1
else
    fclose(fid);
end
