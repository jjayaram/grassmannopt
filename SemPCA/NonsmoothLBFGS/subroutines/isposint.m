function ipi = isposint(x)
% return true if x is a positive integer, false otherwise 
if ~isscalar(x) 
    ipi = 0;
else % following is OK since x is scalar
    ipi = isreal(x) & round(x) == x & x > 0;
end