function p = update_lbfgs(ss,ys,rho,g,cs)

gam = ss(:,cs)'*ys(:,cs) / sum(ys(:,cs).^2);
q   = g;
a   = zeros(1,cs);
for j = cs:-1:1
    a(j) = rho(j)*ss(:,j)'*q;
    q    = q - a(j)*ys(:,j);
end
p = gam*q;
for i = 1:1:cs
    b = rho(i)*ys(:,i)'*p;
    p = p + ss(:,i)*(a(i)-b);
end
p = -p;