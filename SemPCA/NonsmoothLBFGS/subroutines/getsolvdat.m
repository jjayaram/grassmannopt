function s = getsolvdat(sn)

% given the name of a solver sn (string)
% return struct with data about that solver
% possibilities for sn are currently
% 'lbfgs', 'bfgs', 'lmbm', 'redistprox', 'shorrmod', 'bfgsV', 'gradsamp'
%


switch sn
    case 'lbfgs'
        s.col     = 'r';
        s.altcol  = 'm';
        s.symb    = '^';
        s.prtpars = {'ops.m','ops.gnormtol','ops.stpsztol'};
        s.outvars = {};
        s.prtname = 'LBFGS';
        s.prtnamelong = 'Limited Memory BFGS';

    case 'bfgs'
        s.col = 'b';
        s.altcol  = 'g';
        s.symb    = 'x';
        s.prtpars = {'ops.gnormtol','ops.stpsztol'};
        s.outvars = {};
        s.prtname = 'BFGS';
        s.prtnamelong = 'BFGS';

    case 'bfgsV'
        s.col = 'b';
        s.altcol = 'g';
        s.symb = 'x';
        s.prtpars = {'ops.gnormtol','ops.stpsztol'};
        s.outvars = {};
        s.prtname = 'BFGSe';
        s.prtnamelong = 'BFGS with eig-output';

    case 'redistprox'
        s.col = 'm';
        s.symb = '+';
        s.prtpars = {};
        s.outvars = {};
        s.prtname = 'RedistProx';
        s.prtnamelong = 'Redistributed Proximal Bundle (QP = MATLAB)';

    case 'redistproxM'
        s.col = 'm';
        s.symb = '+';
        s.prtpars = {};
        s.outvars = {};
        s.prtname = 'RedistProx';
        s.prtnamelong = 'Redistributed Proximal Bundle (QP = MOSEK)';

    case 'lmbm'
        s.col = 'k';
        s.symb = '*';
        s.prtpars = {'ops.na','ops.mcu','ops.mc'};
        s.outvars = {};
        s.prtname = 'LMBM';
        s.prtnamelong = 'Limited Memory Bundle';

    case 'shorrmod'
        s.col = 'g';
        s.symb = '>';
        s.prtpars = {'ops.gnormtol'};
        s.outvars = {};
        s.prtname = 'ShorRLesage';
        s.prtnamelong = 'Lesage''s Modified Shor-R Algorithm';

    case 'shorr'
        s.col = 'c';
        s.symb = '<';
        s.prtpars = {'ops.gnormtol'};
        s.outvars = {};
        s.prtname = 'ShorR';
        s.prtnamelong = 'Std. Shor-R Algorithm';

    case 'gradsamp'
        s.col = 'y';
        s.symb = 'o';
        s.prtpars = {};
        s.outvars = {};
        s.prtname = 'GradSamp';
        s.prtnamelong = 'Gradient Sampling';

    otherwise
        error('unknown solver');

end
s.runfunc = [sn,'1run'];