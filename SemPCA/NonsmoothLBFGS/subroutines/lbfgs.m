function [x, f, rundata] = lbfgs(pars, ops)

pars.solver = 'lbfgs';
[x, f, rundata] = solverwrap(pars, ops);
