function printfunc(type,ops,pars,rd)

mgsp1 = min(floor(log10(ops.maxit)),6);
switch type
    case 0
        fgnamefid = fopen([pars.fgname,'.m'],'r');
        for i = 1:7
            s{i}=fgetl(fgnamefid);
        end
        fclose(fgnamefid);
        
        nx0t   = 5;
        x0str1 = ['  x_0',spcs(15),'= ['];
        x0str2 = ',...';
        if pars.nvar < nx0t
            nx0t   = pars.nvar;
            x0str2 = '';
        end
        for j = 1:nx0t-1
            x0str1 = [x0str1,num2str(ops.x0(j),'%0.2g'),', '];
        end
        x0str1 = [x0str1,num2str(ops.x0(nx0t),'%0.2g'),x0str2,']\n'];
        slv    = getsolvdat(pars.solver);
        
        if ops.prtlvl > 0
            fprintf(ops.fid,'==============================STA');
            fprintf(ops.fid,'RT===============================\n');
            fprintf(ops.fid,[slv.prtname,' on ']);
            if strcmp(s{3},'% printthis:')
                fprintf(ops.fid,'\n');
                fprintf(ops.fid,[spcs(2),s{4}(3:end),'\n']);
                fprintf(ops.fid,[spcs(6),s{5}(3:end),'\n']);
                nparams = str2double(s{6}(3));
                if nparams>0
                    fprintf(ops.fid,[spcs(2),s{6}(3:end),'\n']);
                end
                if nparams == 1, sh = 1; else sh = 0; end
                for i = 1:nparams
                    if s{7}(6+3*i)=='y'
                        paramname = s{6}(14+3*i-sh);
                        paramval  = eval(['pars.',paramname]);
                        fprintf(ops.fid,[spcs(6),paramname,' = ']);
                        fprintf(ops.fid,[num2str(paramval),'\n']);
                    end
                end
            else
                fprintf(ops.fid,[spcs(0),pars.fgname,'\n']);
            end
            fprintf(ops.fid,[spcs(2),'# Variables',spcs(7),'= ']);
            fprintf(ops.fid,[num2str(pars.nvar),'\n']);
            fprintf(ops.fid,'OPTIONS: \n');
            fprintf(ops.fid,'  max. iterations   = %i \n',ops.maxit);
            fprintf(ops.fid,'  f target value    = %1.4e \n',ops.fvalquit);
            tn1 = length(slv.prtpars);
            for i = 1:tn1
                ts1 = slv.prtpars{i}; cn = length(ts1);
                ts1 = [ts1,spcs(18-cn),'= '];            %#ok<AGROW>
                parval = eval([slv.prtpars{i},';']);
                fprintf(ops.fid,[spcs(2),ts1,num2str(parval,'%1.4e'),'\n']);
            end
            fprintf(ops.fid,x0str1);
            if ops.prtlvl > 1
                fprintf(ops.fid,'---------------------------------');
                fprintf(ops.fid,'---------------------------------\n');
            end
        end
        
    case 1
        if ops.prtlvl > 1
            fprintf(ops.fid,['#i',spcs(mgsp1+1),'nf',spcs(mgsp1+1),...
                'fval',spcs(4+ops.dg),'gnorm',spcs(3+ops.dg),'alpha',...
                spcs(3+ops.dg),'stpsz',spcs(3+ops.dg),'time (s)','\n']  );
            fprintf(ops.fid,'---------------------------------');
            fprintf(ops.fid,'---------------------------------\n');
        end
    case 2
        %mgsp1 = min(floor(log10(ops.maxit)),6);
        %mgsp2 = min(floor(log10(3*ops.maxit)),6);
        if ops.prtlvl > 1
            if rd.fs(rd.k)<0, shf=1; else shf=0; end
            tmpstr1 = spcs(mgsp1-floor(log10(rd.k)));
            tmpstr2 = spcs(mgsp1-floor(log10(rd.nfeval))-shf);
            
            istr  = [' ',num2str(rd.k),tmpstr1];
            nfstr = [num2str(rd.nfeval,'%1.5g'),tmpstr2];
            
            fprintf(ops.fid,[istr,spcs(1),nfstr,spcs(2),...
                ['%1.',num2str(ops.dg),'e'],spcs(2),...
                ['%1.',num2str(ops.dg),'e'],spcs(2),...
                ['%1.',num2str(ops.dg),'e'],spcs(2),...
                ['%1.',num2str(ops.dg),'e']],...
                rd.fs(rd.k),rd.gnorms(rd.k),...
                rd.alphas(rd.k), rd.stpszs(rd.k) );
        end
        
    case 3
        if ops.prtlvl > 0
            
        end
    case 4
        if ops.prtlvl > 1
            fprintf(ops.fid,[spcs(2),'%1.',num2str(ops.dg),'e \n'],rd.times(rd.k));
        end
    otherwise
        
end