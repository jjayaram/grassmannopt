function [x, f, rundata] = bfgs(pars, ops)

pars.solver = 'bfgs';
[x, f, rundata] = solverwrap(pars, ops);
