function [f,g] = nucnorm(x, pars)

% printthis:
% Nuclear-norm problem:
% f = sum of all singular values of X = mat(x) plus rho*||Ax-b||_2
% 0 parameters: p, q, r, alpha, A, b
% show: y, y, y, y, n, n

if isfield(pars,'alpha')
    alpha = pars.alpha;
else
    alpha = 1;
end

p = pars.p; % assume p >= q, where X is p by q
q = pars.q;
A = pars.A;
b = pars.b;
rho = pars.r;
X = reshape(x,p,q);

[U,S,V] = svd(X,0); % economy-sized version
resid = A*x-b;
normresid = norm(resid);
f = sum( diag(S).^alpha ) + rho*normresid;

G = alpha*U*(diag(diag(S).^(alpha-1)))*V';
g = reshape(G,p*q,1);
g = g + rho*A'*resid/normresid;
