function [f,g] = tiltednormcond(x,pars)

% printthis:
% Tilted and conditioned norm function:
% f = w ||A*x||_p + (w-1)<e1,A*x>
% 3 parameters: A, w, p
% show: n, y, y

w  = pars.w;
p  = pars.p;
A  = pars.A;

Ax  = A*x;
nAx = norm(Ax,p);

f = w*nAx + (w-1)*Ax(1);

g = w*x'*A'*A / nAx + (w-1)*A(1,:);
g = g';




