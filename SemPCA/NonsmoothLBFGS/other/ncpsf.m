function [f,g] = ncpsf(x,pars)

% printthis:
% Non-convex partly smooth function:
% f = sqrt( e + sqrt(x'*A*x) + w*x'*(B*x) );
% 4 parameters: e, w, A, B
% show: y, y, n, n

e = pars.e;
w = pars.w;
A = pars.A;
B = pars.B;

Ax  = A*x;
xAx = x'*Ax;
Bx  = B*x;

f  = sqrt(e+sqrt(xAx) + w*x'*(Bx));
g1 = (1/sqrt(xAx))*Ax + 2*Bx;
g  = 1/(2*f)*g1;

