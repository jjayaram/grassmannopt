function [f, g, s, X, Y] = greiffun(x, pars)
% call:  [f, g, s, X, Y] = greiffun(x, pars)
% Chen Greif's problem
% return the condition number and its gradient of
% a matrix of EITHER the form A + XX', where A = pars.A is fixed and
% the variables in the vector x represent the n x r matrix X, 
% OR the form A + XY' (details in nsgreiffun.m)
if ~isfield(pars,'param') % this is consistent with previous usage
    pars.param = 2; % default is symmetric parameterization
end
if pars.param == 1
    error('symmetric indefinite parameterization XDX* not yet implemented')
end
if pars.param == 0
    [f, g, s, X, Y] = nsgreiffun(x, pars);
    return
end
if ~isfield(pars, 'norm')
    matnorm = 2;
else
    matnorm = pars.norm;
end
% symmetric semidefinite parameterization
Y = nan; % not a valid output in the symmetric case
A = pars.A;
n = pars.n;
r = pars.r;
m = n*r;  % m = n*r + 1;
if m ~= pars.nvar
    error('pars.nvar should be pars.n*pars.r')
end
if n ~= length(A)
    error('pars.n should be dimension of A')
end
X = reshape(x(1:n*r), n, r);
XXt = X*X';
M = A + XXt;
if matnorm == 2
    [U,S,V] = svd(M); % equivalently, use eig, since M is psd
    s = diag(S);
    s1 = S(1,1); sn = S(n,n);
    u1 = U(:,1); un = U(:,n);
    v1 = V(:,1); vn = V(:,n);
    f = s1/sn;   % condition number
    % now its gradient
    I = eye(size(A));
    for i = 1:n             
       for j = 1:r    % derivative with respect to X(i,j)
          w = X(:,j);          
          e = I(i,:);                  
          % the derivative of XX' wrt X(ij) is the matrix we + e'w' 
          % where w is jth column of X and e = ith row of I
          D = w*e + e'*w';
          G(i,j) = (u1'*D*v1)/sn - s1*(un'*D*vn)/(sn^2);
       end
    end
elseif matnorm == 1 % one norm condition number
    I = eye(size(A));
    Minv = inv(M);
    [f1, j1] = max(sum(abs(M))); % one norm is largest column sum of abs
    E1 = sign(M(:,j1))*I(j1,:); % derivative of norm given by signs of M in this column
    E1 = (E1 + E1')/2;
    [f2, j2] = max(sum(abs(Minv))); % same for inverse
    E2 = sign(Minv(:,j2))*I(j2,:); 
    E2 = (E2 + E2')/2;
    E2 = -Minv*E2*Minv;  % derivative of norm of inverse (Minv is symmetric)
    f = f1*f2;  % condition number
    % now its gradient
    for i = 1:n             
       for j = 1:r    % derivative with respect to X(i,j)
          w = X(:,j);          
          e = I(i,:);                  
          % the derivative of XX' wrt X(ij) is the matrix we + e'w' 
          % where w is jth column of X and e = ith row of I
          D = w*e + e'*w';
          G(i,j) = f1*E2(:)'*D(:) + f2*E1(:)'*D(:);
       end
    end
    s = nan;
else
    error('matnorm %d is not implemented')
end
g = G(:);   % turn from matrix into vector
