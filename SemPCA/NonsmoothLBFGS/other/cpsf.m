function [f,g] = cpsf(x,pars)

% printthis:
% Convex partly smooth function:
% f = sqrt( x'*A*x ) + w*x'*(B*x);
% 3 parameters: w, A, B
% show: y, n, n


w = pars.w;
A = pars.A;
B = pars.B;

Ax  = A*x;
xAx = x'*Ax;
Bx  = B*x;

f = sqrt(xAx) + w*x'*(Bx);
g = (1/sqrt(xAx))*Ax + 2*Bx;