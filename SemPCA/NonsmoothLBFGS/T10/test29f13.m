function [f,g] = test29f13(x,pars)

% printthis:
% Function 13 from TEST29:
% f = 
% 0 parameter: 
% show: n

g = zeros(pars.nvar,1);
y = [-14.4,-6.8,-4.2,-3.2]';
f = 0;
n = pars.nvar;

for ka = 1:(2*n-4)
    
   i = 2*floor((ka+3)/4)-2;
   l = mod((ka-1),4)+1;
   fa = -y(l);
   for k = 1:3
       a = k^2/l;
       for j = 1:4
           if x(i+j)==0
               x(i+j) = 1e-16;
           end
           a = a*sign(x(i+j))*abs(x(i+j))^(j/(k*l));
       end
       fa = fa + a;
   end
   f  = f + abs(fa);
   ai = sign(fa);
   for k =1:3
       a = k^2/l;
       for j = 1:4
           if x(i+j) == 0
               x(i+j) = 1e-16;
           end
           a = a*sign(x(i+j))*abs(x(i+j))^(j/(k*l));
       end
       for j = 1:4
           g(i+j) = g(i+j)+(j/(k*l))*a/(x(i+j))*ai;
       end
   end
end 