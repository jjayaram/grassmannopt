function [f,g] = test29f17(x,pars)

% printthis:
% Function 17 from TEST29:
% f = 
% 0 parameter: 
% show: n

n = pars.nvar;

g = zeros(n,1);
k = 1;
f = 0;
for ka = 1:n
    j  = floor((ka-1)/5);
    fa = 5-(j+1)*(1-cos(x(ka)))-sin(x(ka));
    j  = 5*j;
    for i = (j+1):(j+5)
        fa = fa-cos(x(i));
    end
    if abs(fa) > f
        k  = ka;
        ai = sign(fa);
        f  = abs(fa);
    end
end
j = floor((k-1)/5);
g(k) = ai*(-(j+1)*sin(x(k))-cos(x(k)));
j = 5*j;
for i = (j+1):(j+5)
    if i == k
        g(i) = g(i) + sin(x(i))*ai;
    else
        g(i) = sin(x(i))*ai;
    end
end

