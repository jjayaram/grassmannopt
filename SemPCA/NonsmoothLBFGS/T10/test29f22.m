function [f,g] = test29f22(x,pars)

% printthis:
% Function 22 from TEST29:
% f = 
% 0 parameter: 
% show: n

n = pars.nvar;
g = zeros(n,1);
f = 0;
k = 1;
U = 1/(n+1);
for ka = 1:n
    
    V = ka*U;
    fa = 2*x(ka)+0.5*U^2*(x(ka)+V+1)^3;
    if ka > 1
        fa = fa - x(ka-1);
    end
    if ka < n
        fa = fa - x(ka+1);
    end
    if f < abs(fa)
        k = ka;
        ai = sign(fa);
        f = abs(fa);
    end
end
V = k*U;
g(k) = (2+1.5*U^2*(x(k)+V+1)^2)*ai;
if k > 1
    g(k-1) = -ai;
end
if k < n
    g(k+1) = -ai;
end

