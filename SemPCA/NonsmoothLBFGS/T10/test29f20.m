function [f,g] = test29f20(x,pars)

% printthis:
% Function 20 from TEST29:
% f = 
% 0 parameter: 
% show: n

n = pars.nvar;
g = zeros(n,1);
f = 0;

k = 1;
for ka = 1:n
    
    if ka < 2
        fa = x(ka)*(0.5*x(ka)-3)-1+2*x(ka+1);
    elseif ka < n
        fa = x(ka-1)+x(ka)*(0.5*x(ka)-3)-1+2*x(ka+1);
    else
        fa = x(ka-1)+x(ka)*(0.5*x(ka)-3)-1;
    end
    if f < abs(fa)
        k = ka;
        ai = sign(fa);
        f = abs(fa);
    end
end
if k < 2
    g(k) = (x(k)-3)*ai;
    g(k+1) = 2*ai;
elseif k < n
    g(k-1)=ai;
    g(k) = (x(k)-3)*ai;
    g(k+1) = 2*ai;
else
    g(k-1) = ai;
    g(k) = (x(k)-3)*ai;
end

