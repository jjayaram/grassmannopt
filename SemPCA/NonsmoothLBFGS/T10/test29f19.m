function [f,g] = test29f19(x,pars)

% printthis:
% Function 19 from TEST29:
% f =
% 0 parameter:
% show: n

n = pars.nvar;
g = zeros(n,1);
f = 0;
k = 1;
H = 2;
for ka = 1:n
    if ka == 1
        fa = ((3-H*x(1))*x(1)-2*x(2)+1)^2;
    elseif ka <= n-1
        fa = ((3-H*x(ka))*x(ka)-x(ka-1)-2*x(ka+1)+1)^2;
    else
        fa = ((3-H*x(n))*x(n)-x(n-1)+1)^2;
    end
    if f<abs(fa)
        k  = ka;
        ai = sign(fa);
        f  = abs(fa);
    end
end

if k == 1
    g(1) = (2*((3-H*x(1))*x(1)-2*x(2)+1)*(3-2*H*x(1)))*ai;
elseif k <= n-1
    g(k-1) = (-2*((3-H*x(k))*x(k)-x(k-1)-2*x(k+1)+1))*ai;
    g(k)   = (2*((3-H*x(k))*x(k)-x(k-1)-2*x(k+1)+1)*(3-2*H*x(k)))*ai;
    g(k+1) = (-4*((3-H*x(k))*x(k)-x(k-1)-2*x(k+1)+1))*ai;
else
    g(n-1) = (-2*((3-H*x(n))*x(n)-x(n-1)+1))*ai;
    g(n)   = (2*((3-H*x(n))*x(n)-x(n-1)+1)*(3-2*H*x(n)))*ai;
end
