function [pars,ops] = getprob_t10(pn,pars,ops)

ops.x0 = zeros(pars.nvar,1);
switch pn

    case 1
        pars.fgname = 'test29f02';
        ops.foptval = 0;
        n2          = ceil(pars.nvar/2);
        ops.x0(1:1:(n2-1)) = (1:1:(n2-1))/pars.nvar;
        ops.x0(n2:1:pars.nvar) = -(n2:1:pars.nvar)/pars.nvar;

    case 2
        pars.fgname = 'test29f05';
        ops.foptval = 0;
        ops.x0      = ones(pars.nvar,1);

    case 3
        pars.fgname = 'test29f06';
        ops.foptval = 0;
        ops.x0      = -ones(pars.nvar,1);

    case 4
        pars.fgname = 'test29f11';
        switch pars.nvar
            case 10
                ops.foptval = 1.0196142930662566e+02; %(fundet med bfgs)
            case 50
                ops.foptval = 5.839002190072002e+02; %(fundet med bfgs)
            case 200
                ops.foptval = 2.391170197531187e+03; %(fundet med bfgs)
            case 1000
                ops.foptval = 1.203127727121690e+04;
            case 5000
                ops.foptval = 6.022596207689382e+04;
            case 10000
                ops.foptval = 0;
        end
        ops.x0      = 0.5*ones(pars.nvar,1);
        ops.x0(pars.nvar) = -2;

    case 5
        % pars.nvar SKAL vaere lige!
        if mod(pars.nvar,2)~=0
            fprintf('WARNING: pars.nvar must be even for test29f13\n');
            fprintf('Setting pars.nvar = pars.nvar-1\n');
            pars.nvar = pars.nvar - 1;
        end

        pars.fgname = 'test29f13';
        v1 = 4:4:pars.nvar;
        v2 = 1:4:pars.nvar;
        v3 = 2:4:pars.nvar;
        v4 = 3:4:pars.nvar;
        ops.x0(v1) = 0.8;
        ops.x0(v2) = -0.8;
        ops.x0(v3) = 1.2;
        ops.x0(v4) = -1.2;
        switch pars.nvar
            case 10
                ops.foptval = 4.537977937095843; %(fundet med bfgs)
            case 50
                ops.foptval = 27.227867622575189; %(fundet med lbfgs)
            case 200
                ops.foptval = 1.123149539431228e+02; %(fundet med bfgs)
            case 1000
                ops.foptval = 5.661313161773337e+02;
            case 5000
                ops.foptval = 2.843636521029362e+03;
        end

    case 6
        % mod(pars.nvar,5) SKAL vaere 0
        if mod(pars.nvar,2)~=0
            fprintf('WARNING: mod(pars.nvar,5) must be 0 for test29f17\n');
            fprintf('Setting pars.nvar = pars.nvar-mod(pars.nvar,5)\n');
            pars.nvar = pars.nvar - mod(pars.nvar,5);
        end
        pars.fgname = 'test29f17';
        ops.foptval = 0;
        ops.x0      = (1+1)*ones(pars.nvar,1)/pars.nvar;

    case 7
        pars.fgname = 'test29f19';
        ops.foptval = 0;
        ops.x0      = -ones(pars.nvar,1);

    case 8
        pars.fgname = 'test29f20';
        ops.foptval = 0;
        ops.x0      = -ones(pars.nvar,1);

    case 9
        pars.fgname = 'test29f22';
        ops.foptval = 0;
        v  = (1+1e-2)*(1:pars.nvar)';
        v1 = v./(pars.nvar+1);
        ops.x0 = v1.*(v1-1);

    case 10
        pars.fgname = 'test29f24';
        pars.P      = 10;
        pars.H      = 1/(pars.nvar+1);
        ops.foptval = 0;
        ops.x0      = ones(pars.nvar,1);

end



