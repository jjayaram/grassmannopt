function [f,g] = test29f24(x,pars)

% printthis:
% Function 24 from TEST29:
% f = 
% 0 parameter: 
% show: n

P = pars.P;
H = pars.H;
n = pars.nvar;
g = zeros(n,1);
f = 0;
k = 1;


for ka = 1:n
    if ka < 2
        fa = 2*x(ka)+P*H^2*sinh(P*x(ka))-x(ka+1);
    elseif ka<n
        fa = 2*x(ka)+P*H^2*sinh(P*x(ka))-x(ka-1)-x(ka+1);
    else
        fa = 2*x(ka)+P*H^2*sinh(P*x(ka))-x(ka-1)-1;
    end
    if f<abs(fa)
        k = ka;
        ai = sign(fa);
        f = abs(fa);
    end
end
if k < 2
    g(k)   = (2+P^2*H^2*cosh(P*x(k)))*ai;
    g(k+1) = -ai;
elseif k < n
    g(k-1) = -ai;
    g(k)   = (2+P^2*H^2*cosh(P*x(k)))*ai;
    g(k+1) = -ai;
else
    g(k)   = (2+P^2*H^2*cosh(P*x(k)))*ai;
    g(k-1) = -ai;
end

