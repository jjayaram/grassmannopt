function [f,g] = test29f02(x,pars)

% printthis:
% Function 2 from TEST29:
% f = max_i abs(x_i)
% 0 parameter: 
% show: n

g       = zeros(pars.nvar,1);
[f,idx] = max(abs(x));
g(idx)  = fsign(1,x(idx));
