function [f,g] = test29f05(x,pars)

% printthis:
% Function 5 from TEST29:
% f =
% 0 parameter:
% show: n

v = (1:pars.nvar)';
[v1,v2] = meshgrid(v);
M = v1 + v2 - 1;

fa = sum(repmat(x,1,pars.nvar)./M);
f  = sum(abs(fa));
ai = sign(fa);
g  = sum(repmat(ai,pars.nvar,1)./M,2);