function [f,g] = test29f06(x,pars)

% printthis:
% Function 6 from TEST29:
% f = 
% 0 parameter: 
% show: n

n = pars.nvar;
g = zeros(n,1);
k = 1;
f = 0;
for i = pars.nvar
    fa = (3-2*x(i))*x(i) + 1;
    if i > 1
        fa = fa-x(i-1);
    end
    if i < pars.nvar
        fa = fa-x(i+1);
    end
    if f <= abs(fa)
        k  = i;
        ai = sign(fa);
        f  = abs(fa);
    end
end
g(k) = (3-4*x(k))*ai;
if k > 1
    g(k-1) = -ai;
end
if k < n
    g(k+1) = -ai;
end