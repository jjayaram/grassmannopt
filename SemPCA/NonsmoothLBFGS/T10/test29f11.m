function [f,g] = test29f11(x,pars)

% printthis:
% Function 11 from TEST29:
% f = 
% 0 parameter: 
% show: n

g = zeros(pars.nvar,1);
f = 0; n = pars.nvar;

for ka = 1:(2*n-2)
    
   i = floor((ka+1)/2);
   if mod(ka,2) == 1
       fa = x(i) + x(i+1)*((5-x(i+1))*x(i+1)-2)-13;
       ai = sign(fa);
       g(i) = g(i) + ai;
       g(i+1) = g(i+1) + (10*x(i+1)-3*x(i+1)^2-2)*ai;
   else
       fa = x(i)+x(i+1)*((1+x(i+1))*x(i+1)-14)-29;
       ai = sign(fa);
       g(i) = g(i) + ai;
       g(i+1) = g(i+1) + (2*x(i+1)+3*x(i+1)^2-14)*ai;
   end
   f = f + abs(fa);
    
end
