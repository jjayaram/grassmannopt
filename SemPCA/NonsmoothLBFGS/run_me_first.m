% assuming you leave the files in the locations they were
% when the zip-file was unpacked, this will add the right paths
% to your MATLABPATH, allowing you to run all the algorithms
% (Running LMBM requires that you contruct a mex-file - see instructions)
% (Running ShorRLesage and RedistProx requires that you get them
%  from the authors) -- see website. 
                                           %     
thisdir = cd; maindir = thisdir;	   % get this dir
addpath(genpath(maindir));                 % add all dirs to MATLABPATH
