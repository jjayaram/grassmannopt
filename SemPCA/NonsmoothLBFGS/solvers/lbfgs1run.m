function [x, f, rd] = lbfgs1run(pars, ops)

% Initialize:
rd.curstate = 0;
tic;
x     = ops.x0;
[f,g] = feval(pars.fgname, x, pars);
gnorm = norm(g);


%X     = randn(pars.nvar);
%H0    = X'*X;
%p     = -H0*g;

p     = -g;%/gnorm; % first search direction.
cs    = 0;

rd.fsall    = [];
fsalleval   = f;
rd.nfeval   = 1;
nfevallsrch = 1;
initialchecks_solver(f,g,gnorm,ops);

% printing:
printfunc(1,ops,pars,rd);

for k = 1:ops.maxit
    t1 = toc;
    
    % ---------- gather data --------------
    if ops.gathdata
        rd.xs(:,k)        = x;
        rd.gs(:,k)        = g;
        rd.ps(:,k)        = p;
    end
    rd.fsall(end+1:end+nfevallsrch) = fsalleval;
    rd.k                  = k;
    % ---------- end gather data --------------

    gtp   = g'*p; % negative if p is descent dir
    [rd.curstate,rd.prtstr] = checkfordescentdir_solver(gtp);
    if rd.curstate ~= 0
        rd.ttime = sum(rd.times);
        printfunc(3,ops,pars,rd);
        return
    end
    
    % (Weak Wolfe) line search:
    [alpha, xkp1, f, gkp1, rd.lsfail,beta,gbeta,nfevallsrch,fsalleval] = ...
        linesch_ww(x, f, g, p, pars, ops.wolfe1, ops.wolfe2,...
        ops.fvalquit, ops.prtlvl);
    
    xkp1(xkp1<0) = 0;
    rd.nfevalcumu(k) = rd.nfeval;
    rd.fs(k)     = f;
    rd.nfeval    = rd.nfeval+nfevallsrch;
    rd.alphas(k) = alpha;
    gnorm        = norm(gkp1);
    rd.gnorms(k) = gnorm;
    curss        = xkp1 - x;
    rd.stpszs(k) = norm(curss);
    
    % ------------ printing --------------------
    printfunc(2,ops,pars,rd);
    % ------------- end printing ---------------
    
    % ---------- Check current status of algorithm --------
    [rd.curstate,rd.prtstr] = checkstatus_solver(rd,ops,pars);
    if rd.curstate ~= 0
        rd.fs(:,k+1)       = f;
        rd.fsall(end+1:end+nfevallsrch) = fsalleval;
        rd.gnorms(k+1)     = gnorm;
        rd.nfevalcumu(k+1) = rd.nfeval;
        t2                 = toc;
        rd.times(k)        = t2-t1;
        rd.ttime           = sum(rd.times);
        printfunc(4,ops,pars,rd);
        printfunc(3,ops,pars,rd);
        if ops.gathdata
            rd.xs(:,k+1)   = xkp1;
            rd.gs(:,k+1)   = gkp1;
        end
        return
    end
    % ------------------------------------------------------
    
    % ----- variables for L-BFGS update: ---------
    cs        = cs + 1;
    ss(:,cs)  = curss;
    ys(:,cs)  = gkp1 - g;
    rho(cs)   = 1/(ys(:,cs)'*ss(:,cs));
    if cs > ops.m
        ss(:,1) = [];
        ys(:,1) = [];
        rho(1)  = [];
        cs      = cs-1;
    end
    
    % ----- L-BFGS UPDATE: ----------------------
    p = update_lbfgs(ss,ys,rho,gkp1,cs);
    % ------- end L-BFGS UPDATE -----------------------
    
    x = xkp1;
    g = gkp1;    
    t2 = toc;
    rd.times(k) = t2-t1;
    printfunc(4,ops,pars,rd);
    
end % for loop

