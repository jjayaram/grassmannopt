% Decomposing a similarity graph into multiple "semantic principal
% components" - the input similarity matrix is converted to symmetric prob
% matrix everytime the weights are updated

% Load data
addpath('../data');
addpath('../ICASSP2015/tSNE_matlab');
load mnist_4digs_small;
X = test_X';

% Choose a small number of components for speed
%X = X(:,1:400);

% Path for LBFGS solver
addpath(genpath('NonsmoothLBFGS'));
addpath(genpath('/Users/karthikeyan/Github/grassmannopt/ICASSP2015/Jan5_2015/DR'));

% Preprocess data
preproc = true;
if (preproc)
    X = preProcData(X');
end

d = 2; % Low-dimensional embedding
N = size(X,2); % No. of nodes in the similarity graph
M= 4; % No. of SemPCA components
maxiter = 10;
lambda = 0.0; % penalty while computing embedding

% Compute (or input) similarity
% Try to Modify the W here to reflect symmetric similarities and rerun
% the code...
sigma = 0.25;
perplexity = 30;
DX = L2_distance(X,X).^2;
DX = DX/max(max(DX));
W = exp(-DX/(2*(sigma^2)));
W = (W+W')/2;
% P = d2p(DX, perplexity, 1e-5);
% W = (P+P')/(2*N);

%ydata = tsne(test_X);
%load YTSNE_mnist_2dig;
ydata = TSNE_4digs_small;

figure;
for lidx = 1:length(test_labels)
    text(ydata(lidx,1),ydata(lidx,2),num2str(test_labels(lidx))); hold on;
end
xlim([min(ydata(:,1)) max(ydata(:,1))]); ylim([min(ydata(:,2)) max(ydata(:,2))]);
hold off; title('TSNE Embedding');

% Dm = diag(sum(W));
% Dmsq = diag(1./sqrt(sum(W)));
% [U1 S1] = eig(diag(sum(W))-W,Dm);
% [U2 S2] = eig(eye(size(X,2))-Dmsq*W*Dmsq);
% figure; scatter(U1(:,2),U1(:,3));
% figure; scatter(U2(:,2),U2(:,3));


% [mappedX, Xupd] = lapEMOrig_1(X', 2, 3950, sigma);
% ydata = mappedX;
% figure;
% for lidx = 1:length(test_labels)
%     text(ydata(lidx,1),ydata(lidx,2),num2str(test_labels(lidx))); hold on;
% end
% xlim([min(ydata(:,1)) max(ydata(:,1))]); ylim([min(ydata(:,2)) max(ydata(:,2))]);
% hold off;

% Init embeddings
Y = zeros(d,N,M);
D = zeros(N,N,M);
% Init node weights
Tau = 10*rand(N,M);
Tau_init = Tau;

% Set LBFGS options
lbfgs_options;

% % Initial cost
% % Distance computation (for the embedding)
% for m = 1:M
%     D(:,:,m) = L2_distance(Y(:,:,m),Y(:,:,m)).^2;
% end
% pars.D      = D;
% pars.W      = W;
% pars.Tau    = Tau;
% pars.m      = m;
% % Iteratively optimize for embeddings and node weights
% costfn(1) = SemPCACostGrad_Tau(Tau(:,1),pars);

costfn = zeros(maxiter,1);
allinds = (1:d*M);
ProbsDiff = zeros(maxiter,1);
for i1 = 1:maxiter
    % Compute node probs
    Pi = exp(-Tau);
    Pi = bsxfun(@times,Pi,1./sum(Pi,2));
    % Regularization to force node probs to small pos val
    Pi = Pi+(1e-10);
    Pi = bsxfun(@times,Pi,1./sum(Pi,2));
    
    Pi'*Pi
    ProbsDiff(i1) = sum(sum(Pi'*Pi))-sum(diag(Pi'*Pi));
    
    % Compute reweighted similarities for all the components
    Pi1 = shiftdim(Pi,-1);
    Wrw = bsxfun(@times,W,Pi1);
    Pi2 = permute(Pi1,[2,1,3]);
    Wrw = bsxfun(@times,Wrw,Pi2);
    
    Yr = reshape(permute(Y,[2 1 3]),[N d*M]);
    % Each component embedding obtained iteratively
    twopartobj = true;
    if (~twopartobj)
        for m = 1:M
            A = Yr(:,setdiff(allinds,(m-1)*d+1:m*d));
            Di = 1./sqrt(sum(Wrw(:,:,m)));
            B = eye(N)-bsxfun(@times,bsxfun(@times,Wrw(:,:,m),Di),Di');
            
            B = (B+B')/2;
            [Ys, eigvals] = eig(B+lambda*(A*A'));
            [svals, sinds] = sort(diag(eigvals),'ascend');
            %,d+1,'SA');
            Yr(:,(m-1)*d+1:m*d) = Ys(:,sinds(2:(d+1)));
        end
    else
        for m = 1:M
            A = Yr(:,setdiff(allinds,(m-1)*d+1:m*d));
            DW = sum(Wrw(:,:,m));
            Di = 1./sqrt(DW);
            B = diag(DW)-Wrw(:,:,m);
            
            B = (B+B')/2;
            % Degree normalization step - may be causing issues: NRK
            %[Ys, eigvals] = eig(bsxfun(@times,bsxfun(@times,...
                                %B+lambda*(A*A'),Di),Di'));
            [Ys, eigvals] = eig(B+lambda*(A*A'));
            %[Ys,eigvals] = DNM_TR(diag(Di),B,d+1,'full');
            %Yr(:,(m-1)*d+1:m*d) = Ys(:,1:d);
            [svals, sinds] = sort(diag(eigvals),'ascend');
            Yr(:,(m-1)*d+1:m*d) = Ys(:,sinds(2:(d+1)));
        end        
    end
    
    Y = permute(reshape(Yr,[N d M]),[2 1 3]);
    
    % Plot the component embeddings
    figure;
    for m = 1:M
        Yemb = Yr(:,(m-1)*d+1:m*d);
        subplot(2,2,m); scatter(Yemb(:,1),Yemb(:,2),100*Pi(:,m)); hold on;
        for lidx = 1:length(test_labels)
            text(Yemb(lidx,1),Yemb(lidx,2),num2str(test_labels(lidx))); hold on;
        end
        xlim([min(Yemb(:,1)) max(Yemb(:,1))]); ylim([min(Yemb(:,2)) max(Yemb(:,2))]);
        hold off;
        title(['component embedding - ',num2str(m),', iteration - ',num2str(i1)]);
    end
    
    %close(100);
    
    % Distance computation (for the embedding)
    for m = 1:M
        D(:,:,m) = L2_distance(Y(:,:,m),Y(:,:,m)).^2;
    end
    
    % Each node weight vector obtained iteratively
    optimopts = optimoptions(@fminunc,'GradObj','on');
    for m = 1:M
        % Objective parameters
        pars.fgname = 'SemPCACostGrad_Tau';
        pars.D      = D;
        pars.W      = W;
        pars.Tau    = Tau;
        pars.m      = m;
        pars.nvar   = N;
        options.x0  = Tau(:,m);
        [Tau(:,m),cost,rd] = lbfgs(pars,options);
        %[Tau(:,m),cost] = fminunc(@(x)SemPCACostGrad_Tau(D,W,Tau,m,x),Tau(:,m),optimopts);
    end
    
    % Objective value
    costfn(i1) = cost;
end

figure; plot(costfn);