function [mappedX, Xupd] = lapEMOrig_1(X, no_dims, k, sigma)
if ~exist('no_dims', 'var')
    no_dims = 2;
end
if ~exist('k', 'var')
    k = 12;
end
if ~exist('sigma', 'var')
    sigma = 1;
end


% Construct neighborhood graph
if size(X, 1) < 4000
    G = L2_distance(X', X');
    
    % Compute neighbourhood graph
    [tmp, ind] = sort(G);
    for i=1:size(G, 1)
        G(i, ind((2 + k):end, i)) = 0;
    end
    G = sparse(double(G));
    G = max(G, G');             % Make sure distance matrix is symmetric
else
    G = find_nn(X, k);
end
G = G .^ 2;
mapping.max_dist = max(max(G));
G = G ./ mapping.max_dist;

% Only embed largest connected component of the neighborhood graph
blocks = components(G)';
count = zeros(1, max(blocks));
for i=1:max(blocks)
   count(i) = length(find(blocks == i));
end
[count, block_no] = max(count);
conn_comp = find(blocks == block_no);
G = G(conn_comp, conn_comp);

% Compute weights (W = G)
% Compute Gaussian kernel (heat kernel-based weights)
G(G ~= 0) = exp(-G(G ~= 0) / (2 * sigma ^ 2));

% Construct diagonal weight matrix
D = diag(sum(G, 2));

% Compute Laplacian
L = D - G;
L(isnan(L)) = 0; D(isnan(D)) = 0;
L(isinf(L)) = 0; D(isinf(D)) = 0;

% Construct eigenmaps (solve Ly = lambda*Dy)
tol = 0;

options.disp = 0;
options.isreal = 1;
options.issym = 1;
[mappedX, lambda] = eigs(L, D, no_dims + 1, tol, options);
[mappedX, lambda] = eig(full(L), full(D));

% Sort eigenvectors in ascending order
lambda = diag(lambda);
[lambda, ind] = sort(lambda, 'ascend');
lambda = lambda(2:no_dims + 1)

% Final embedding
mappedX = mappedX(:,ind(2:no_dims + 1));
Xupd = X(conn_comp,:)';