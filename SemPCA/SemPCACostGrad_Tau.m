function [f,g] = SemPCACostGrad_Tau(Taum,pars)

D = pars.D;
W = pars.W;
Tau = pars.Tau;
m = pars.m;

N = size(W,2);
M = size(D,3);

Tau(:,m) = Taum;
Pi = exp(-Tau);
Pi = bsxfun(@times,Pi,1./sum(Pi,2));
assert (all(size(Pi) == [N M]));

R = bsxfun(@times,D,W);
Pi1 = shiftdim(Pi,-1);
RPi1 = bsxfun(@times,R,Pi1);
Pi2 = permute(Pi1,[2,1,3]);
RPi2 = bsxfun(@times,RPi1,Pi2);

% function value
f = sum(sum(sum(RPi2)));

X = squeeze(sum(RPi1,2));

% gradient
g = 2*Pi(:,m).*(sum(Pi.*X,2)-X(:,m));