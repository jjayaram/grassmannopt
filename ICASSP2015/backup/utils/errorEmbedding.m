function [e, p_j_i, q_j_i] = errorEmbedding(DFull,DEmb,pv,gamma,lambda)
% TODO: Use the entropy approach proposed in Venna et. al. to set gamma
% based on entropy of p_j_i (j \neq i)
% Compute the smoothed precision and recall for the embedidng

% normalize probabilities
% pv = pv/sum(pv); 

T = size(DEmb,1);

DFull = bsxfun(@times,bsxfun(@times,exp(-gamma*DFull),pv),pv');
% DEmb = bsxfun(@times,bsxfun(@times,exp(-gamma*DEmb),pv),pv');
DEmb = exp(-gamma*DEmb);

p_j_i = bsxfun(@times,DFull,1./(sum(DFull,2)-diag(DFull)));
q_j_i = bsxfun(@times,DEmb,1./(sum(DEmb,2)-diag(DEmb)));

eMat = lambda*p_j_i.*log(p_j_i./q_j_i)+(1-lambda)*q_j_i.*log(q_j_i./p_j_i);
e = sum(eMat,2)-diag(eMat);

entr_p_j_i = p_j_i.*log(p_j_i);
entr_p_j_i = nansum([-nansum(entr_p_j_i,2) -diag(entr_p_j_i)],2);
me = nanmean(entr_p_j_i);

end