A = randn(5,2);
B = orth(A);

B'*B % Must be I

[C,D1] = eig(B*B')

C'*C % must be I

B'*C % Need not be I