% Compute the embedding of subspaces computed

% Compute the embedding of subspaces
[YS,D] = embedSubspaces(Vw);
figure; scatter(YS(:,1),YS(:,2),[],'MarkerEdgeColor','none');

for i1 = 1:iter
    text(YS(i1,1),YS(i1,2),num2str(i1),'FontSize',10);
end
