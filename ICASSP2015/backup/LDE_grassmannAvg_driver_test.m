% LPP with grassmann averaged subspaces on test data

% load the file from training phase
addpath('data');
addpath('utils');
load('USPS_LDE_ensemble.mat');

% Size of test data
Tte = size(Xte,2);

% Testing full LDE
Yte = V'*Xte;
distMatFullte = L2_distance(Xte,Xte);
distMatEmbte = L2_distance(Yte,Yte);

% Compute the error measure for embedding
errte = errorEmbedding(distMatFullte,distMatEmbte,ones(Tte,1),gamma,lambda);
errtes = mean(errte);

% Predictions with single embeddings
LabTePredF = predict(knnFull,Xte');
LabTePredRS = predict(knnRedSing,Yte');
fprintf('Class. Perf. with full data - 1 emb. (test) = %.4f \n',sum(LabTe == LabTePredF)/length(LabTe));
fprintf('Class. Perf. with projected data - 1 emb. (test) = %.4f \n',sum(LabTe == LabTePredRS)/length(LabTe));

if ~isempty(VAM)
    % Embedding with the arithmetic mean subspace
    YAMte = VAM'*Xte;
    distMatEmbAMte = L2_distance(YAMte,YAMte);
    
    % Compute the error with arithmetic mean subspace
    errAMte = errorEmbedding(distMatFullte,distMatEmbAMte,ones(Tte,1),gamma,lambda);
    errAMtes = mean(errAMte);
    
    % Prediction with AM subspace
    LabTePredAM = predict(knnRedAM,YAMte');
    fprintf('Class. Perf. with AM subspace embedding (test) = %.4f \n',sum(LabTe == LabTePredAM)/length(LabTe));
end

% Embedding with the grassmannian mean subspace
YGMte = VGM'*Xte;
distMatEmbGMte = L2_distance(YGMte,YGMte);

% Compute the error with extrinsic mean subspace
errGMte = errorEmbedding(distMatFullte,distMatEmbGMte,ones(Tte,1),gamma,lambda);
errGMtes = mean(errGMte);

% Test Classification error with GM subspace embedding
LabTePredGM = predict(knnRedGM,YGMte');
fprintf('Class. Perf. with GM subspace embedding (test) = %.4f \n',sum(LabTe == LabTePredGM)/length(LabTe));

fprintf('Embedding error with non-randomized full LDE (test) = %.4f \n',errtes);
if ~isempty(VAM)
    fprintf('Embedding error with projections computed with AM subspace (test) = %.4f \n',errAMtes);
end
fprintf('Embedding error with projections computed with GM subspace (test) = %.4f \n',errGMtes);