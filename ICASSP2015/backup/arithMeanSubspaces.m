function VAM = arithMeanSubspaces(Vw,d_arr)
% Arithmetic mean of subspaces

if ~all(d_arr==mean(d_arr))
    warning('Cannot compute arithmetic mean on subspaces of different dimensions');
    VAM = [];
    return;
else
    iter = length(d_arr);
    for i1 = 1:iter
        if (i1 == 1)
            Vmean1 = (1/iter)*Vw{i1};
        else
            Vmean1 = Vmean1+(1/iter)*Vw{i1};
        end
    end
end