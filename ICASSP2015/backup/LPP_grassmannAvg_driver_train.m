% LPP with incoherent subspace learning and grassmann averaging

%% Set necessary parameters and initializations
addpath('data');
addpath('utils');

load usps_all;
[M,Ttot] = size(Xtot_pca);

% Create train and test sets
prop = 0.7; %training proportion
totindr = randperm(Ttot);
T = floor(prop*Ttot);
Tte = Ttot-T;
trind = totindr(1:T);
teind = totindr(T+1:Ttot);

X = Xtot_pca(:,trind);
Xte = Xtot_pca(:,teind);

iter = 4; % randomizing iterations
k = 5; % nearest neighbors
dMax = 3; % max. number of projections
dMin = 3; % min. number of projections
d = dMax; %ceil((dMax+dMin)/2); % another option is to set it at dMax
randProj = 0; % whether or not to randomize projection dimensions
gamma = 0.1; % parameter for smoothed error measure
lambda = 0.1; % tradeoff between smoothed precision and recall
alpha = 0; %1e8; % Tradeoff parameter to compute the new subspace

if randProj
    d_arr = randsample((dMin:1:dMax)',iter,true);
else
    d_arr = d*ones(iter,1);
end

%% Training phase
% Compute unweighted adjacency and Laplacian matrices
[G,~,~] = adjacency_NNGraph_binary(X,ones(T,1),k,0);

% Full LPP
V = computeUnsupProj(X,G,d);
Y =V'*X;
distMatFull = L2_distance(X,X);
distMatEmb = L2_distance(Y,Y);

% Compute the error measure for embedding
err = errorEmbedding(distMatFull,distMatEmb,ones(T,1),gamma,lambda);
errs = mean(err);

% Create multiple incoherent subspaces
[Vw, errws]  = LPP_incohSubspaces(X,G,d_arr,alpha,gamma,lambda,distMatFull,1);

% Arithmetic mean of subspaces
% VAM = arithMeanSubspaces(Vw,d_arr);

% if ~isempty(VAM)
%     % Embedding with the arithmetic mean subspace
%     YAM = VAM'*X;
%     distMatEmbAM = L2_distance(YAM,YAM);
%     
%     % Compute the error with arithmetic mean subspace
%     errAM = errorEmbedding(distMatFull,distMatEmbAM,ones(T,1),gamma,lambda);
%     errAMs = mean(errAM);
% end

% Grassmannian (chordal) mean of subspaces
VGM = weightedMeanChordal(Vw,(1/iter)*ones(iter,1));

% Embedding with the extrinsic mean subspace
YGM = VGM'*X;
distMatEmbGM = L2_distance(YGM,YGM);

% Compute the error with extrinsic mean subspace
errGM = errorEmbedding(distMatFull,distMatEmbGM,ones(T,1),gamma,lambda);
errGMs = mean(errGM);

fprintf('Error with non-randomized LPP = %.4f \n',errs);
if ~isempty(VAM)
    fprintf('Mean error with all randomizations arithmetic mean subspace = %.4f \n',errAMs);
end
fprintf('Error with projections computed with extrinsic mean subspace = %.4f \n',errGMs);

save('USPS_LPP_ensemble','Xte','k','V','VAM','VGM','gamma','lambda');