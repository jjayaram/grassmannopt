function [ydata w] = combTSNECost(P, Y, labels)
% Linearly combine the preliminary embeddings by optimizing the weights
% using tSNE cost function

if ~exist('labels', 'var')
    labels = [];
end

% Initialize some variables
n = size(P, 1);                                     % number of instances
momentum = 0.5;                                     % initial momentum
final_momentum = 0.8;                               % value to which momentum is changed
mom_switch_iter = 250;                              % iteration at which momentum is changed
stop_lying_iter = 100;                              % iteration at which lying about P-values is stopped
max_iter = 1000;                                    % maximum number of iterations
epsilon = 100;                                      % initial learning rate
min_gain = .01;                                     % minimum gain for delta-bar-delta
no_dims = size(Y{1},2);
T = length(Y);

% reshape the data - n x L x no_dims
Ym = permute(reshape(cell2mat(Y'),[n,no_dims,T]),[1 3 2]);

% Make sure P-vals are set properly
P(1:n + 1:end) = 0;                                 % set diagonal to zero
P = 0.5 * (P + P');                                 % symmetrize P-values
P = max(P ./ sum(P(:)), realmin);                   % make sure P-values sum to one
const = sum(P(:) .* log(P(:)));                     % constant in KL divergence
P = P * 4;                                          % lie about the P-vals to find better local minima

% Initialize the weights and ydata
w = rand(T,1);
w = w/sum(w);
ydata = zeros(n,no_dims);
for i1 = 1:no_dims
    ydata(:,i1) = Ym(:,:,i1)*w;
end
ydata = bsxfun(@minus, ydata, mean(ydata, 1));

w_incs  = zeros(size(w));
gains = ones(size(w));

% Run the iterations
for iter=1:max_iter    
    % Compute joint probability that point i and j are neighbors
    sum_ydata = sum(ydata .^ 2, 2);
    num = 1 ./ (1 + bsxfun(@plus, sum_ydata, bsxfun(@plus, sum_ydata', -2 * (ydata * ydata')))); % Student-t distribution
    num(1:n+1:end) = 0;                                                 % set diagonal to zero
    Q = max(num ./ sum(num(:)), realmin);                               % normalize to get probabilities
    
    % Compute the gradients (faster implementation)
    L = (P - Q) .* num;
    w_grads = zeros(T,1);
    for t = 1:T
        YYt = ydata*squeeze(Ym(:,t,:))';
        YYts = bsxfun(@plus,YYt(1:n+1:end)',YYt(1:n+1:end))-YYt-YYt';
        w_grads(t) = -4*sum(sum(L.*YYts)); % sign reversed
    end
    
    % Update the solution
    gains = (gains + .2) .* (sign(w_grads) ~= sign(w_incs)) ...         % note that the y_grads are actually -y_grads
            + (gains * .8) .* (sign(w_grads) == sign(w_incs));
    gains(gains < min_gain) = min_gain;
    w_incs = momentum * w_incs - epsilon * (gains .* w_grads);
    w = w + w_incs;
    for i1 = 1:no_dims
        ydata(:,i1) = Ym(:,:,i1)*w;
    end
    ydata = bsxfun(@minus, ydata, mean(ydata, 1));
    
    % Update the momentum if necessary
    if iter == mom_switch_iter
        momentum = final_momentum;
    end
    if iter == stop_lying_iter
        P = P ./ 4;
    end
    
    % Print out progress
    if ~rem(iter, 10)
        cost = const - sum(P(:) .* log(Q(:)));
        disp(['Iteration ' num2str(iter) ': error is ' num2str(cost)]);
    end
    
    % Display scatter plot (maximally first three dimensions)
    if ~rem(iter, 10) && ~isempty(labels)
        if no_dims == 1
            scatter(ydata, ydata, 9, labels, 'filled');
        elseif no_dims == 2
            scatter(ydata(:,1), ydata(:,2), 9, labels, 'filled');
        else
            scatter3(ydata(:,1), ydata(:,2), ydata(:,3), 40, labels, 'filled');
        end
        axis tight
        axis off
        drawnow
    end
end