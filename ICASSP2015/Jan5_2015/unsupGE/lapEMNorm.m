function [mappedX] = lapEMNorm(P, no_dims)
% Laplacian eigenmaps with normalized probabilities (inspired by SNE)
% P - input p_j|i matrix of probabilities

if ~exist('no_dims', 'var')
    no_dims = 2;
end

% Construct diagonal weight matrix
D = diag(sum(P, 2));

% Compute Laplacian
L = D - P;
L(isnan(L)) = 0; D(isnan(D)) = 0;
L(isinf(L)) = 0; D(isinf(D)) = 0;

% Construct eigenmaps (solve Ly = lambda*Dy)
tol = 0;

options.disp = 0;
options.isreal = 1;
options.issym = 1;
[mappedX, lambda] = eigs(L, D, no_dims + 1, tol, options);
[mappedX, lambda] = eig(L, D);

% Sort eigenvectors in ascending order
lambda = diag(lambda);
[lambda, ind] = sort(lambda, 'ascend');
lambda = lambda(2:no_dims + 1)

% Final embedding
mappedX = mappedX(:,ind(2:no_dims + 1));