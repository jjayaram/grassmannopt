function Gu = findNeigh(X,k)

N = size(X,2);
Iu = zeros(N*k*5,1);
Ju = Iu;
Countu = 1;

% Go in jumps of 1000 to speed up the computations
blocksize = 1000;
for j1 = 1:blocksize:N
    blockids = (j1:min(j1+blocksize-1,size(X,2)));
    
    DMatb = L2_distance(X,X(:,blockids));
    
    for i1 = 1:length(blockids)
        DVect = DMatb(:,i1);
        
        % Class unaware
        [sval sind] = sort(DVect,'ascend');
        Iu(Countu:Countu+k) = blockids(i1);
        Ju(Countu:Countu+k) = sind(1:k+1);
        Countu = Countu+k+1;
    end
end

Countu = Countu-1;
Iu = Iu(1:Countu);
Ju = Ju(1:Countu);

Gu = sparse(Iu,Ju,1,N,N);