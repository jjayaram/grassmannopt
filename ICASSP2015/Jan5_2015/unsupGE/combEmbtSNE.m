function [ydata w] = combEmbtSNE(X,YLPPc,labels,perplexity)
% Combine the embeddings using some form of averaging and a tSNE cost
% function

% Intialize some parameters

if ~exist('labels', 'var')
    labels = [];
end
if ~exist('perplexity', 'var') || isempty(perplexity)
    perplexity = 30;
end

% Compute pairwise distance matrix
sum_X = sum(X .^ 2, 2);
D = bsxfun(@plus, sum_X, bsxfun(@plus, sum_X', -2 * (X * X')));

% Compute joint probabilities
P = d2p(D, perplexity, 1e-5); % compute affinities using fixed perplexity
clear D

% Combine linearly using tSNE cost
[ydata w] = combTSNECost(P, YLPPc, labels);