% LPP-orig
function [mappedX, mappedXAM, W, mapping] = lppGrass(X,no_dims,numIter,alpha,k,sigma)

T = size(X,1);

if ~exist('no_dims', 'var')
    no_dims = 2;
end
if ~exist('k', 'var')
    k = 12;
end
if ~exist('sigma', 'var')
    sigma = 1;
end

% Construct neighborhood graph
if size(X, 1) < 4000
    G = L2_distance(X', X');
    % Compute neighbourhood graph
    [tmp, ind] = sort(G);
    for i=1:size(G, 1)
        G(i, ind((2 + k):end, i)) = 0;
    end
    G = sparse(double(G));
    G = max(G, G');             % Make sure distance matrix is symmetric
else
    G = find_nn(X, k);
end
G = G .^ 2;
G = G ./ max(max(G));

% Compute weights (W = G)
% Compute Gaussian kernel (heat kernel-based weights)
G(G ~= 0) = exp(-G(G ~= 0) / (2 * sigma ^ 2));

D = diag(sum(G, 2));
% Compute Laplacian
L = D - G;
L(isnan(L)) = 0; D(isnan(D)) = 0;
L(isinf(L)) = 0; D(isinf(D)) = 0;
C = 0;
for iter = 1:numIter
    %fprintf('Iteration %d \n',iter);
    if iter == 1
        % Compute XDX and XLX and make sure these are symmetri
        DP = X' * D * X;
        LP = X' * L * X;
        DP = (DP + DP') / 2;
        LP = (LP + LP') / 2;
    else
        DP = X' * D * X;
        LP = X' * L * X;
        DP = (DP + DP') / 2;
        LP = (LP + LP')/2  + alpha*C;
    end
  
    % Perform eigenanalysis of generalized eigenproblem (as in LEM)
    %if size(X, 1) > 200 && no_dims < (size(X, 1) / 2)
    %    options.disp = 0;
    %    options.issym = 1;
    %    options.isreal = 1;
    %    [eigvector, eigvalue] = eigs(LP, DP, no_dims, 'SA', options);
    %else
    [eigvector, eigvalue] = eig(LP, DP);
    %end
    
    % Sort eigenvalues in descending order and get smallest eigenvectors
    [eigvalue, ind] = sort(diag(eigvalue), 'ascend');
    eigvector = eigvector(:,ind(1:no_dims));
    W{iter} = orth(eigvector);
    C = ((iter-1)/iter)*C + (1/iter)*W{iter}*W{iter}';
end

Wfinal = weightedMeanChordal(W,ones(numIter,1));
% Compute final linear basis and map data
mappedX = X * Wfinal;
mapping.M = Wfinal;
mapping.mean = mean(X, 1);

% Find the arithmetic mean of the representations
for iter = 1:numIter
    if (iter == 1) 
        sr = rand;
        mappedXAM = sr*X*W{iter};
    else
        sa = rand;
        sr = sr+sa;
        mappedXAM = mappedXAM + sa*X*W{iter};
    end
    YLPPIt = X*W{iter};
    %figure; scatter(YLPPIt(:,1),YLPPIt(:,2)); title(['LPP - iteration ', num2str(iter)]);
end
mappedXAM = mappedXAM/sr;