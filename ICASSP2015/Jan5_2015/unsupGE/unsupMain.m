% Unsupervised Graph Embedding - Main Entry
%clear all
%clc
%close all

addpath('data');
addpath(genpath('../utils'));
addpath(genpath('../../tSNE_matlab'));
addpath(genpath('../DR'));

%load ecoli
%load seaWater
%load breast
load facevideo
%data = [];
%for k =[1,3,9,16,20]
%    ids = find(Y==k);
%    rp =randperm(length(ids));
%    data = [data X(ids(rp(1:100)),:)'];
%end
%X = data;
%X = Xtot_pca;
%rp = randperm(size(X,2));
%X = X(:,rp(1:1500));
%X = compute_mapping(faces','PCA',200)';
[M,T] = size(X);

numIter = 20; % randomizing iterations
k = 15; % nearest neighbors 15
d = 2; %ceil((dMax+dMin)/2); % another option is to set it at dMax
gamma = 0.01:0.04:0.5; % parameter for smoothed error measure
lambda = 0.5; % tradeoff between smoothed precision and recal
numNeigh = 50;
alpha = 1e8;
sigma = 1;
preproc = true;

if (preproc)
    X = preProcData(X');
end

% LPP embedding
[YLPP, mapping] = lppOrig(X', d,k);
distMatFull = L2_distance(X,X);
distMatEmb = L2_distance(YLPP',YLPP');
[errLPP,~, ~] = errorEmbedding(distMatFull,distMatEmb,ones(T,1),gamma,lambda);
[precLPP, recLPP] = computePrecRec(X,YLPP',numNeigh);

% LPP + Grassmanian Regularization
[YGMLPP, YAMLPP, W, mapping] = lppGrass(X',d,numIter,alpha,k);
distMatFull = L2_distance(X,X);
distMatEmbGM = L2_distance(YGMLPP',YGMLPP');
distMatEmbAM = L2_distance(YAMLPP',YAMLPP');
[errGMLPP,~, ~] = errorEmbedding(distMatFull,distMatEmbGM,ones(T,1),gamma,lambda);
[errAMLPP,~, ~] = errorEmbedding(distMatFull,distMatEmbAM,ones(T,1),gamma,lambda);
[precGMLPP, recGMLPP] = computePrecRec(X,YGMLPP',numNeigh);
[precAMLPP, recAMLPP] = computePrecRec(X,YAMLPP',numNeigh);

% LapEigMap embedding
[YLEM, Xupd] = lapEMOrig(X',d,1960);
distMatFull = L2_distance(X,X);
distMatEmb = L2_distance(YLEM',YLEM');
[errLEM,~, ~] = errorEmbedding(distMatFull,distMatEmb,ones(T,1),gamma,lambda);
[precLEM, recLEM] = computePrecRec(Xupd,YLEM',numNeigh);

% LapEigMap with normalized input probs
sum_X = sum((X') .^ 2, 2);
D = bsxfun(@plus, sum_X, bsxfun(@plus, sum_X', -2 * (X' * X)));
P = d2p(D,50,1e-5);
P = (P+P')/2;
[YLEMN] = lapEMNorm(P,d);
distMatEmb = L2_distance(YLEMN',YLEMN');
[errLEMN,~, ~] = errorEmbedding(distMatFull,distMatEmb,ones(T,1),gamma,lambda);
[precLEMN, recLEMN] = computePrecRec(X,YLEMN',numNeigh);


% NPE
[YNPE] = npeOrig(X',d,k);
distMatFull = L2_distance(X,X);
distMatEmb = L2_distance(YNPE',YNPE');
[errNPE,~, ~] = errorEmbedding(distMatFull,distMatEmb,ones(T,1),gamma,lambda);
[precNPE, recNPE] = computePrecRec(X,YNPE',numNeigh);

% NPE + Grassmanian Regularization
[YGMNPE] = npeGrass(X',d,numIter,alpha,k);
distMatFull = L2_distance(X,X);
distMatEmb = L2_distance(YGMNPE',YGMNPE');
[errGMNPE,~, ~] = errorEmbedding(distMatFull,distMatEmb,ones(T,1),gamma,lambda);
[precGMNPE, recGMNPE] = computePrecRec(X,YGMNPE',numNeigh);

%fprintf('Error with non-randomized LPP = %.4f \n',err);
%fprintf('Error with projections computed with extrinsic mean subspace = %.4f \n',errGM);

% Embed using tSNE
%YtSNE = tsne(X');
YtSNE = sne(X',2,15);
%YtSNE = tsne(X',Y);
distMatFull = L2_distance(X,X);
distMatEmb = L2_distance(YtSNE',YtSNE');
[errLPP,~, ~] = errorEmbedding(distMatFull,distMatEmb,ones(T,1),gamma,lambda);
[precTSNE, recTSNE] = computePrecRec(X,YtSNE',numNeigh);

%%% ============ Main modification ==================
% LPP+arithmetic mean with t-SNE cost function
if (false)
L = length(W);
YLPPc = cell(L,1);
for l = 1:L
    YLPPc{l} = X'*W{l};
end
%Weighted linear combination of various embeddings
%YLPPc = cell(4,1);
%YLPPc{1} = YLPP; YLPPc{2} = YLEM; YLPPc{3} = YNPE; YLPPc{4} = YtSNE;

[YcombLPP, w] = combEmbtSNE(X',YLPPc,Y);
distMatEmbComb = L2_distance(YcombLPP',YcombLPP');
[errComb,~, ~] = errorEmbedding(distMatFull,distMatEmbComb,ones(T,1),gamma,lambda);
[precComb, recComb] = computePrecRec(X,YcombLPP',numNeigh);
end
%%% ============ End Main modification ==================

figure; plot(recLPP,precLPP,'ro--');
hold on; plot(recGMLPP,precGMLPP,'ro-');
plot(recAMLPP,precAMLPP,'go--');
plot(recLEM,precLEM,'co--');
plot(recNPE,precNPE,'bo--');
plot(recGMNPE,precGMNPE,'bo-');
plot(recTSNE,precTSNE,'mo-');
%plot(recComb,precComb,'kv-');
plot(recLEMN,precLEMN,'yo-');

legend('LPP','LPP (Proposed)','LPP (Arith Mean)','LapEigMap','NPE',...
    'NPE (Proposed)','t-SNE','LapEigMap\_Norm'); %'combTSNE'
grid on;
xlabel('Mean Recall'); ylabel('Mean Precision');

if (true)
    figure; scatter(YLPP(:,1),YLPP(:,2)); title('LPP');
    figure; scatter(YGMLPP(:,1),YGMLPP(:,2)); title('LPP (Proposed)');
    figure; scatter(YAMLPP(:,1),YAMLPP(:,2)); title('LPP (Arith Mean)');
    figure; scatter(YLEM(:,1),YLEM(:,2)); title('LapEigMap');
    figure; scatter(YNPE(:,1),YNPE(:,2)); title('NPE');
    figure; scatter(YGMNPE(:,1),YGMNPE(:,2)); title('NPE (Proposed)');
    figure; scatter(YtSNE(:,1),YtSNE(:,2)); title('t-SNE');
    %figure; scatter(YcombLPP(:,1),YcombLPP(:,2)); title('CombEmb_t-SNE');
    figure; scatter(YLEMN(:,1),YLEMN(:,2)); title('LapEigMap\_Norm');
end