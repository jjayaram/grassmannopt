% Comparison of different methods - Unsupervised

methods = {'Isomap','LTSA','tSNE', 'LPP', 'NPE'};

addpath('data');
addpath('utils');

load usps_all;
% X = compute_mapping(faces'/255,'PCA',200)';
% X = Xtot_pca;
% load olivettifaces;
% Xtot_pca = compute_mapping(faces'/255,'PCA',200)';
rp = randperm(size(X,1));
X = X(rp(1:1500),:)';
labs = Y(rp(1:1500));

X = Xtot_pca;

[M,T] = size(X);
d = 10; % Number of embedding dimensions
cmap = hsv(length(methods)+1);

lambda = 0.5;
gamma = 0.01:0.03:0.5;

for i = 1:length(methods)
    [Y, mapping] = compute_mapping(X', methods{i}, d);
    if size(Y,1) < size(X,2)
        distMatFull = L2_distance(X(:,mapping.conn_comp),X(:,mapping.conn_comp));
        %relab = labs(mapping.conn_comp);
        %mdl = ClassificationKNN.fit(Y,relab,'NumNeighbors',5);
    else
        distMatFull = L2_distance(X,X);
        %relab = labs;
        %mdl = ClassificationKNN.fit(Y,labs,'NumNeighbors',5);
    end
    distMatEmb = L2_distance(Y',Y');
    
    
    %labPred = predict(mdl,Y);
    
   % clErr(i) = sum(labPred ~= relab)/length(labPred);
    
%     cl = hsv(10);
%     for k = 1:10
%         ids = find(C==k);
%         figure(i+10); hold on; scatter(Y(ids,1),Y(ids,2),40,cl(k,:));
%         title(methods{i});
%     end
    
    [e,smR, smP] = errorEmbedding(distMatFull,distMatEmb,ones(size(distMatFull,1),1),gamma,lambda);
    figure(2); hold on; plot(gamma,e,'Color',cmap(i,:),'MarkerSize',10,'Marker','o',...
    'LineWidth',2);
    title(['Smoothed Error metric (d = ' num2str(d) ')']);
end
plot(gamma,errGM,'Color',cmap(6,:),'MarkerSize',10,'Marker','o',...
   'LineWidth',2);
grid on
legend('Isomap','LTSA','tSNE', 'LPP', 'NPE','Proposed')

% cl = hsv(10);
% for k = 1:10
%     ids = find(C==k);
%     figure(i+10); hold on; scatter(YGM(1,ids),YGM(2,ids),40,cl(k,:));
%     title(methods{i});
% end




