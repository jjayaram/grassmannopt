function [precM, recM] = measPerf(Ytr, Yte, labTr, labTe)

T = size(Yte,2);
for k = unique(labTr)'
    nSamp(k) = sum(labTr==k);
end
i = 1;
H = L2_distance(Yte,Ytr);
[H1, ind] = sort(H,2,'ascend');

for k = 1:30:min(500,size(Ytr,2))
    ids = ind(:,1:k);
    TP = sum(labTr(ids)==repmat(labTe,1,k),2);
    precM(i) = mean(TP/k);
    recM(i) = mean(TP./nSamp(labTe)');
    i = i +1;
end
