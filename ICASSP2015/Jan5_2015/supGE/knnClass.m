function perf = knnClass(Ytr, Yte, labTr, labTe)

k = 5;
mdl = ClassificationKNN.fit(Ytr',labTr,'NumNeighbors',5);
labPred = predict(mdl,Yte');

perf = sum(labPred == labTe)./length(labTe);
