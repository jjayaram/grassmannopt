function [mappedX, Wfinal] = ldeGrass(X,lab,d,numIter, alpha, k,kp)

[~,G,Gp] = adjacency_NNGraph_binary(X,lab,k,kp);

DG = diag(sum(G));
L = DG-G;
DGp = diag(sum(Gp));
Lp = DGp-Gp;

A = X*Lp*X';
A = (A+A')/2;

B = X*L*X';
B = (B+B')/2;

C = 0;
for iter = 1:numIter
    BP = B + alpha*C;
    [W{iter},~,ll] = ITR(A,BP,d,'full');
    C = ((iter-1)/iter)*C + (1/iter)*W{iter}*W{iter}';
end
Wfinal = weightedMeanChordal(W,ones(numIter,1));
mappedX = Wfinal'*X;