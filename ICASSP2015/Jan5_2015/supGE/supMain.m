% Unsupervised Graph Embedding - Main Entry
clear all
clc
%close all

addpath('data');
addpath('utils');

load ORL_32x32;
X = compute_mapping(fea,'PCA',200)';
lab = gnd;

prop = 0.7; %training proportion
folds = cvpartition(lab,'holdout',1-prop);
trind = training(folds);
teind = test(folds);

Xtr = X(:,trind);
Xte = X(:,teind);
labTr = lab(trind);
labTe = lab(teind);
T = length(trind);
Tte = length(teind);

numIter = 20; % randomizing iterations
k = 15; % nearest neighbors
d = 30; %ceil((dMax+dMin)/2); % another option is to set it at dMax
alpha = 1e8;

% LDA Embedding
[Ytr, W] = ldaOrig(Xtr', labTr, d);
Yte = W'*Xte;
[precLDA, recLDA] = measPerf(Ytr', Yte, labTr, labTe);
perfLDA = knnClass(Ytr', Yte, labTr, labTe);

% LDA Embedding + Grassmannian Regularization
[Ytr, W] = ldaGrass(Xtr', labTr, length(unique(labTr))-1, numIter,alpha);
Yte = W'*Xte;
[precGMLDA, recGMLDA] = measPerf(Ytr', Yte, labTr, labTe);
perfGMLDA = knnClass(Ytr', Yte, labTr, labTe);


% LDE Embedding
k = 10; kp = 10;
[Ytr, W] = ldeOrig(Xtr, labTr, d, k, kp);
Yte = W'*Xte;
[precLDE, recLDE] = measPerf(Ytr, Yte, labTr, labTe);
perfLDE = knnClass(Ytr, Yte, labTr, labTe);


% LDE Embedding + Grassmannian Regularization
[Ytr, W] = ldeGrass(Xtr, labTr, d, numIter, alpha, k, kp);
Yte = W'*Xte;
[precGMLDE, recGMLDE] = measPerf(Ytr, Yte, labTr, labTe);
perfGMLDE = knnClass(Ytr, Yte, labTr, labTe);

figure; plot(recLDA,precLDA,'ro--');
hold on; plot(recGMLDA,precGMLDA,'ro-');
plot(recLDE,precLDE,'bo--');
plot(recGMLDE,precGMLDE,'bo-');
grid on;