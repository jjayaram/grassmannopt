function W = computeSupProjConstr(X,G,Gp,C,alpha,d)
% X  - data
% G - Adjacency matrix (intra class)
% Gp - Adjacency matrix (inter class)
% C - The next subspace needs to be far from C
% alpha - tradeoff parameter
% d - number of reduced dimensions
% W - projection matrix

DG = diag(sum(G));
L = DG-G;
DGp = diag(sum(Gp));
Lp = DGp-Gp;

A = X*Lp*X';
A = (A+A')/2;

B = X*L*X';
B = (B+B')/2 +alpha*C;

% Iterative trace ratio optimization
[ W,~,ll] = ITR(A,B,d,'full');

% fprintf('Numerator trace is %f \n',trace(W'*X*Lp*X'*W));
% fprintf('Denominator trace-1 is %f \n',trace(W'*X*L*X'*W));
% fprintf('Denominator trace-2 is %f \n',alpha*trace(W'*C*W));