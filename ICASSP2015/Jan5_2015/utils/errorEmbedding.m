function [eM, smRecMean, smPrecMean, p_j_i, q_j_i] = errorEmbedding(DFull,DEmb,pv,gamma,lambda)
% TODO: Use the entropy approach proposed in Venna et. al. to set gamma
% based on entropy of p_j_i (j \neq i)
% Compute the smoothed precision and recall for the embedidng

% normalize probabilities
% pv = pv/sum(pv);

T = size(DEmb,1);
nVar = length(gamma);
D1 = DFull;
D2= DEmb;

for n = 1:nVar
    DFull = D1;
    DEmb = D2;
    
    DFull = exp(-gamma(n)*DFull); %bsxfun(@times,bsxfun(@times,exp(-gamma(n)*DFull),pv),pv');
    % DEmb = bsxfun(@times,bsxfun(@times,exp(-gamma*DEmb),pv),pv');
    DEmb = exp(-gamma(n)*DEmb);
    
    p_j_i = bsxfun(@times,DFull,1./(sum(DFull,2)-diag(DFull)));
    q_j_i = bsxfun(@times,DEmb,1./(sum(DEmb,2)-diag(DEmb)));
    
    eMat = lambda*p_j_i.*log(p_j_i./q_j_i)+(1-lambda)*q_j_i.*log(q_j_i./p_j_i);
    e = sum(eMat,2)-diag(eMat);
    eM(n) = mean(e);
    
    smRec = p_j_i.*log(p_j_i./q_j_i);
    smRecMean(n) = mean(sum(smRec,2) - diag(smRec));
    
    smPrec = q_j_i.*log(q_j_i./p_j_i);
    smPrecMean(n) = mean(sum(smPrec,2) - diag(smPrec));
    
   % entr_p_j_i = p_j_i.*log(p_j_i);
   % entr_p_j_i = nansum([-nansum(entr_p_j_i,2) -diag(entr_p_j_i)],2);
   % me = nanmean(entr_p_j_i);
end
