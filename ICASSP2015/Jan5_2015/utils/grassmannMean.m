function M = grassmannMean(A,B,f)
% Grassmann mean of two subspaces (equal dimensions)

[k,n] = size(A);

[U,G,Q] = svd(((eye(n)-A*A')*B)/(A'*B));
theta = atan(diag(G));

M = A*U*diag(cos(f*theta))+Q*diag(sin((1-f)*theta));