function [YS,D] = embedSubspaces(Vw)
% Embed the subspaces in Vw in a low dimensional space

% Embedding dimensions (for visualization)
d = 2;
gamma = 0.1; % scale param
k = 3; % # of NN

T = length(Vw);
D = zeros(T,T);

% Distance matrix
for i2 = 1:T
    for i3 = i2+1:T
        D(i2,i3) = distChordalGrass(Vw{i2},Vw{i3});
    end
end
D = (D+D').^2;

% Compute an embedding with D
G = zeros(T,T);
for i2 = 1:T
    [sval,sinds] = sort(D(:,i2),'ascend');
    G(sinds(1:k+1),i2) = 1;
end
G = (G+G') > 0;
D1 = (G > 0).*exp(-gamma*D); % Heat kernel weights
D1 = exp(-gamma*D);

% Laplacian eigenmaps
Deg = diag(sum(D1));
L = Deg-D1;
[YS,lambda] = eigs(L,Deg,d+1,'sa');

YS = YS(:,2:d+1);
lambda = diag(lambda);
[lambda, ind] = sort(lambda, 'ascend');
lambda = lambda(2:d + 1);