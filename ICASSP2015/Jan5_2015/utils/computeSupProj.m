function W = computeSupProj(X,G,Gp,d)
% X  - data
% G - Adjacency matrix (intra class)
% Gp - Adjacency matrix (inter class)
% d - number of reduced dimensions
% W - projection matrix

DG = diag(sum(G));
L = DG-G;
DGp = diag(sum(Gp));
Lp = DGp-Gp;

A = X*Lp*X';
A = (A+A')/2;

B = X*L*X';
B = (B+B')/2;

% Iterative trace ratio optimization
[ W,~,ll] = ITR(A,B,d,'full');