function W = computeUnsupProjConstr(X,G,C,alpha,d)
% X  - data
% G - Adjacency matrix
% C - The next subspace needs to be far from C
% alpha - tradeoff parameter
% d - number of reduced dimensions
% V - projection matrix

Deg = diag(sum(G));
L = Deg-G;

A = X*Deg*X';
A = (A+A')/2;

B = X*L*X';
B = (B+B')/2 + alpha*C;

% Iterative trace ratio optimization
[ W,~,ll] = ITR(A,B,d,'full');

% fprintf('Numerator trace is %f \n',trace(W'*X*Deg*X'*W));
% fprintf('Denominator trace-1 is %f \n',trace(W'*X*L*X'*W));
% fprintf('Denominator trace-2 is %f \n',alpha*trace(W'*C*W));