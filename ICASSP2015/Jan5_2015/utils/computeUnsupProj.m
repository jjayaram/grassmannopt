function W = computeUnsupProj(X,G,d)
% X  - data
% G - Adjacency matrix
% d - number of reduced dimensions
% V - projection matrix

Deg = diag(sum(G));
L = Deg-G;

A = X*Deg*X';
A = (A+A')/2;

B = X*L*X';
B = (B+B')/2;

% Iterative trace ratio optimization
[ W,~,ll] = ITR(A,B,d,'full');

