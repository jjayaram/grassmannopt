function [precM, recM] = computePrecRec(X,Y)

T = size(Y,2);
G = findNeigh(X,50) - eye(T);
i = 1;
for k = 1:20:301
    disp(k);
    H = findNeigh(Y,k) - eye(T);
    TP = sum(G.*H,2);
    FP = sum((1-G).*H,2);
    FN = sum(G.*(1-H),2);
    
    precM(i) = mean(TP./(TP + FP));
    recM(i) = mean(TP./(TP + FN));
    i = i +1;
end



