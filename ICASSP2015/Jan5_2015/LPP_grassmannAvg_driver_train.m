% LPP with incoherent subspace learning and grassmann averaging

%% Set necessary parameters and initializations
addpath('data');
addpath(genpath('utils'));

load olivettifaces;
Xtot_pca = compute_mapping(faces'/255,'PCA',200)';
%rp = randperm(size(X,1));
%Xtot_pca = X(rp(1:2000),:)';
%labs = Y(rp(1:1500));

[M,Ttot] = size(Xtot_pca);

% Create train and test sets
prop = 1; %training proportion
totindr = randperm(Ttot);
T = floor(prop*Ttot);
Tte = Ttot-T;
trind = totindr(1:T);
teind = totindr(T+1:Ttot);

X = Xtot_pca(:,trind);
Xte = Xtot_pca(:,teind);

iter = 50; % randomizing iterations
k = 20; % nearest neighbors
dMax = 40; % max. number of projections
dMin = 20; % min. number of projections
d = 2; %ceil((dMax+dMin)/2); % another option is to set it at dMax
randProj = 0; % whether or not to randomize projection dimensions
gamma = 0.01:0.03:0.5; % parameter for smoothed error measure
lambda = 0; % tradeoff between smoothed precision and recall
alpha = 1e15; % Tradeoff parameter to compute the new subspace

if randProj
    d_arr = randsample((dMin:1:dMax)',iter,true);
else
    d_arr = d*ones(iter,1);
end

%% Training phase
% Compute unweighted adjacency and Laplacian matrices
%[G,~,~] = adjacency_NNGraph_binary(X,ones(T,1),k,0);
if size(X, 2) < 4000
    G = L2_distance(X, X);
    % Compute neighbourhood graph
    [tmp, ind] = sort(G);
    for i=1:size(G, 1)
        G(i, ind((2 + k):end, i)) = 0;
    end
    G = sparse(double(G));
    G = max(G, G');             % Make sure distance matrix is symmetric
else
    G = find_nn(X', k);
end

sigma = 1;
G(G ~= 0) = exp(-G(G ~= 0) / (2 * sigma ^ 2));

% Full LPP
V = computeUnsupProj(X,G,d);
Y =V'*X;
distMatFull = L2_distance(X,X);
distMatEmb = L2_distance(Y,Y);

% Compute the error measure for embedding
[err,smRecs, smPrecs] = errorEmbedding(distMatFull,distMatEmb,ones(T,1),gamma,lambda);
%errs = mean(err);
[prec, rec] = computePrecRec(X,Y);

% Create multiple incoherent subspaces
[Vw, errws]  = LPP_incohSubspaces(X,G,d_arr,alpha,gamma,lambda,distMatFull,0);

% Arithmetic mean of subspaces
%VAM = arithMeanSubspaces(Vw,d_arr);

% if ~isempty(VAM)
%     % Embedding with the arithmetic mean subspace
%     YAM = VAM'*X;
%     distMatEmbAM = L2_distance(YAM,YAM);
%     
%     % Compute the error with arithmetic mean subspace
%     [errAM,smRecAMs, smPrecAMs] = errorEmbedding(distMatFull,distMatEmbAM,ones(T,1),gamma,lambda);
%     [precAM, recAM] = computePrecRec(X,YAM);
%     errAMs = mean(errAM);
% end

% Grassmannian (chordal) mean of subspaces
VGM = weightedMeanChordal(Vw,(1/iter)*ones(iter,1));

% Embedding with the extrinsic mean subspace
YGM = VGM'*X;
distMatEmbGM = L2_distance(YGM,YGM);

% Compute the error with extrinsic mean subspace
[errGM, smRecGMs, smPrecGMs] = errorEmbedding(distMatFull,distMatEmbGM,ones(T,1),gamma,lambda);
%errGMs = mean(errGM);
%[precGM, recGM] = computePrecRec(X,YGM);

% fprintf('Error with non-randomized LPP = %.4f \n',errs);
% if ~isempty(VAM)
%     fprintf('Mean error with all randomizations arithmetic mean subspace = %.4f \n',errAMs);
% end
% fprintf('Error with projections computed with extrinsic mean subspace = %.4f \n',errGMs);


figure; plot(gamma,err,'ro--');
hold on; plot(gamma,errGM,'ro-');
legend('Locality Preserving Projections','Grassmannian Regularization');
title(['Smoothed Error metric (d = ' num2str(d) ')']);



