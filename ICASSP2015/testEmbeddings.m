th1 = pi/3;
R1 = [cos(th1) -sin(th1);
      sin(th1) cos(th1)];
[U1, S1, V1] = svd(R1);
Sig1 = U1*diag([1,0.2])*V1';

th2 = 2*pi/3;
R2 = [cos(th2) -sin(th2);
      sin(th2) cos(th2)];
[U2, S2, V2] = svd(R2);
Sig2 = U2*diag([1,0.2])*V2';

figure; scatter(U1(1,1),U1(2,1),'r*');
hold on; scatter(U2(1,1),U2(2,1),'r+');
xlim([-1 1]); ylim([-1 1]);

[U3,S3,V3] = svd([U1*diag([1,0.2]),U2*diag([1,0.2])]);