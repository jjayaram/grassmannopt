function [mappedX, W] = ldeOrig(X,lab,d,k,kp)

[~,G,Gp] = adjacency_NNGraph_binary(X,lab,k,kp);

DG = diag(sum(G));
L = DG-G;
DGp = diag(sum(Gp));
Lp = DGp-Gp;

A = X*Lp*X';
A = (A+A')/2;

B = X*L*X';
B = (B+B')/2;

% Iterative trace ratio optimization
[ W,~,ll] = ITR(A,B,d,'full');

mappedX = W'*X;