% LPP with grassmann averaged subspaces on test data

% load the file from training phase
addpath('data');
addpath('utils');
load('USPS_LPP_ensemble.mat');
gamma = 0.1; % parameter for smoothed error measure
lambda = 0.5; % tradeoff between smoothed precision and recall

% Size of test data
Tte = size(Xte,2);

% Compute unweighted adjacency and Laplacian matrices
[Gte,~,~] = adjacency_NNGraph_binary(Xte,ones(Tte,1),k,0);

% Testing full LPP (no weighting)
Yte = V'*Xte;
distMatFullte = L2_distance(Xte,Xte);
distMatEmbte = L2_distance(Yte,Yte);

% Compute the error measure for embedding
errte = errorEmbedding(distMatFullte,distMatEmbte,ones(Tte,1),gamma,lambda);
errtes = mean(errte);

if ~isempty(VAM)
    % Embedding with the arithmetic mean subspace
    YAMte = VAM'*Xte;
    distMatEmbAMte = L2_distance(YAMte,YAMte);
    
    % Compute the error with arithmetic mean subspace
    errAMte = errorEmbedding(distMatFullte,distMatEmbAMte,ones(Tte,1),gamma,lambda);
    errAMtes = mean(errAMte);
end

% Embedding with the grassmannian mean subspace
YGMte = VGM'*Xte;
distMatEmbGMte = L2_distance(YGMte,YGMte);

% Compute the error with extrinsic mean subspace
errGMte = errorEmbedding(distMatFullte,distMatEmbGMte,ones(Tte,1),gamma,lambda);
errGMtes = mean(errGMte);

fprintf('Error with non-randomized full LPP (test) = %.4f \n',errtes);
if ~isempty(VAM)
    fprintf('Error with projections computed with arithmetic mean subspace (test) = %.4f \n',errAMtes);
end
fprintf('Error with projections computed with extrinsic mean subspace (test) = %.4f \n',errGMtes);