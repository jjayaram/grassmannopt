function A = weightedMeanChordal(B,w)
% Computes the weighted chordal mean of T subspaces represented by n x k_i
% matrices in the cell array B. w is the weight vector.

T = length(w);
w = w/sum(w);
n = size(B{1},1);

Bs = zeros(n,n);
dims = zeros(T,1);

for t = 1:T
    dims(t) = size(B{t},2);
    Bs = Bs+w(t)*eye(n)-w(t)*B{t}*B{t}';
end
k = max(dims);
Bs = 0.5*(Bs+Bs');

[A,~] = eigs(Bs,k,'sa');

% [eigvector, eigvalue] = eig(Bs);
% [eigvalue, ind] = sort(diag(eigvalue), 'ascend');
% A = eigvector(:,ind(1:k));