% Unsupervised Graph Embedding - Main Entry
clear all
clc
%close all

addpath('data');
addpath('utils');

load ecoli
%data = [];
%for k =[1,3,9,16,20]
%    ids = find(Y==k);
%    rp =randperm(length(ids));
%    data = [data X(ids(rp(1:100)),:)'];
%end
%X = data;
%X = Xtot_pca;
%rp = randperm(size(X,2));
%X = X(:,rp(1:1500));
%X = compute_mapping(faces','PCA',200)';
[M,T] = size(X);

numIter = 20; % randomizing iterations
k = 15; % nearest neighbors
d = 2; %ceil((dMax+dMin)/2); % another option is to set it at dMax
gamma = 0.01:0.04:0.5; % parameter for smoothed error measure
lambda = 0.5; % tradeoff between smoothed precision and recal
numNeigh = 50;
alpha = 1e8;
sigma = 1;

% PCA embedding
%[YPCA] = pcaOrig(X', d);
%distMatFull = L2_distance(X,X);
%distMatEmb = L2_distance(YPCA',YPCA');
%[errPCA,~, ~] = errorEmbedding(distMatFull,distMatEmb,ones(T,1),gamma,lambda);
%[precPCA, recPCA] = computePrecRec(X,YPCA',numNeigh);

% LPP embedding
[YLPP, mapping] = lppOrig(X', d,k);
%distMatFull = L2_distance(X,X);
%distMatEmb = L2_distance(YLPP',YLPP');
%[errLPP,~, ~] = errorEmbedding(distMatFull,distMatEmb,ones(T,1),gamma,lambda);
[precLPP, recLPP] = computePrecRec(X,YLPP',numNeigh);

% LPP + Grassmanian Regularization
[YGMLPP, mapping] = lppGrass(X',d,numIter,alpha,k);
%distMatFull = L2_distance(X,X);
%distMatEmb = L2_distance(YGMLPP',YGMLPP');
%[errGMLPP,~, ~] = errorEmbedding(distMatFull,distMatEmb,ones(T,1),gamma,lambda);
[precGMLPP, recGMLPP] = computePrecRec(X,YGMLPP',numNeigh);

% LapEigMap embedding
[YLEM Xupd] = lapEMOrig(X',d,k);
%distMatFull = L2_distance(X,X);
%distMatEmb = L2_distance(YLEM',YLEM');
%[errLEM,~, ~] = errorEmbedding(distMatFull,distMatEmb,ones(T,1),gamma,lambda);
[precLEM, recLEM] = computePrecRec(Xupd,YLEM',numNeigh);

% NPE
[YNPE] = npeOrig(X',d,k);
%distMatFull = L2_distance(X,X);
%distMatEmb = L2_distance(YNPE',YNPE');
%[errNPE,~, ~] = errorEmbedding(distMatFull,distMatEmb,ones(T,1),gamma,lambda);
[precNPE, recNPE] = computePrecRec(X,YNPE',numNeigh);

% NPE + Grassmanian Regularization
[YGMNPE] = npeGrass(X',d,numIter,alpha,k);
%distMatFull = L2_distance(X,X);
%distMatEmb = L2_distance(YGMNPE',YGMNPE');
%[errGMNPE,~, ~] = errorEmbedding(distMatFull,distMatEmb,ones(T,1),gamma,lambda);
[precGMNPE, recGMNPE] = computePrecRec(X,YGMNPE',numNeigh);

%fprintf('Error with non-randomized LPP = %.4f \n',err);
%fprintf('Error with projections computed with extrinsic mean subspace = %.4f \n',errGM);

figure; plot(recLPP,precLPP,'ro--');
hold on; plot(recGMLPP,precGMLPP,'ro-');
%plot(recPCA,precPCA,'go--');
plot(recLEM,precLEM,'co--');
plot(recNPE,precNPE,'bo--');
plot(recGMNPE,precGMNPE,'bo-');
legend('LPP','LPP (Proposed)','LapEigMap','NPE','NPE (Proposed)');
grid on;