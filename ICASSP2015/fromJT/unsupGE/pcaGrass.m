function [mappedX, mapping] = pcaGrass(X, no_dims,numIter,alpha)

if ~exist('no_dims', 'var')
    no_dims = 2;
end

% Make sure data is zero mean
mapping.mean = mean(X, 1);
X = bsxfun(@minus, X, mapping.mean);

% Compute covariance matrix
if size(X, 2) < size(X, 1)
    C = cov(X);
else
    C = (1 / size(X, 1)) * (X * X');        % if N>D, we better use this matrix for the eigendecomposition
end

% Perform eigendecomposition of C
C(isnan(C)) = 0;
C(isinf(C)) = 0;

Z = zeros(size(C));
for iter = 1:numIter
    if iter==1
        [M lambda] = eig(C);
    else
        [M, lambda] = eig(C+alpha*Z);
    end
    [lambda, ind] = sort(diag(lambda), 'descend');
    W{iter} = orth(M(:,ind(1:no_dims)));
    Z = ((iter-1)/iter)*Z + (1/iter)*(no_dims - W{iter}*W{iter}');
    
end

Wfinal = weightedMeanChordal(W,ones(numIter,1));
mappedX = X * Wfinal;


