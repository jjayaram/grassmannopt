% Compute a randomized version of LPP, where the final subspace for projection
% will be computed using a weighted combination of random subspaces, the
% weights themselves being determined by the trace ratios
% (Does not work well)

M = 1000; % dimensions
T = 10000; % samples

X = randn(M,T);

iter = 1000; % #iterations
k = 5; % nearest neighbors
d = 20; % number of projections
gamma = 0.5; % parameter for smoothed error measure
lambda = 0.5; % tradeoff between smoothed precision and recall

% Compute unweighted adjacency and Laplacian matrices
[G,~,~] = adjacency_NNGraph_binary(X,ones(T,1),k,0);

% Necessary matrices
Deg = diag(sum(G));
L = Deg-G;
A = X*Deg*X';
B = X*L*X';

% Full LPP (no weighting)
V = computeUnsupProj(X,G,d);
Y = V'*X;
% distMatFull = L2_distance(X,X);
% distMatEmb = L2_distance(Y,Y);

trace(V'*A*V)/trace(V'*B*V)
% % Compute the error measure for embedding
% err = errorEmbedding(distMatFull,distMatEmb,ones(T,1),gamma,lambda);
% errs = sum(err);

% Subspace projection directions
Vw = cell(iter,1);
trRatio = zeros(iter,1);

for i1 = 1:iter
    % Generate a random subspace
    Vw{i1} = randn(M,d);
    Vw{i1} = orth(Vw{i1});
%     Vw{i1} = Vw{i1}*diag(1./sqrt(sum(Vw{i1}.^2)));
    
    % Compute the trace ratio
    trRatio(i1) = trace(Vw{i1}'*A*Vw{i1})/trace(Vw{i1}'*B*Vw{i1});
end

% weighted average of subspaces
Vmean = weightedMeanChordal(Vw,trRatio);

trace(Vmean'*A*Vmean)/trace(Vmean'*B*Vmean)

% Embedding with the mean subspace
Ymean = Vmean'*X;
distMatEmbMean = L2_distance(Ymean,Ymean);

% Compute the error with weighted data
errmean = errorEmbedding(distMatFull,distMatEmbMean,ones(T,1),gamma,lambda);
errmeans = sum(errmean);

fprintf('Error with non-randomized LPP = %.4f \n',errs);
fprintf('Mean error with all randomizations LPP = %.4f \n',mean(errws));
fprintf('Error with projections computed with mean subspace = %.4f \n',errmeans);