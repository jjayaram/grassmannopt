% LPP with incoherent subspace learning and grassmann averaging

%% Set necessary parameters and initializations
addpath('data');
addpath('utils');

load usps_all;
X = Xtot_pca; Y = C;
Xtr = []; labtr = [];
Xte = []; labte = [];
numTr = 500;
numTe = 100;
for k = 0:9
    ids = find(Y == k);
    rp = randperm(length(ids));
    Xtr = [Xtr X(:,rp(1:numTr))];
    Xte = [Xte X(:,rp(numTr+1:numTr+numTe))];
    labtr = [labtr;k*ones(numTr,1)];
    labte = [labte;k*ones(numTe,1)];
end

[M,T] = size(Xtr);

iter = 50; % randomizing iterations
k = 15; % nearest neighbors
dMax = 40; % max. number of projections
dMin = 20; % min. number of projections
d = 10; %ceil((dMax+dMin)/2); % another option is to set it at dMax
randProj = 0; % whether or not to randomize projection dimensions
lambda = 0.5; % tradeoff between smoothed precision and recall
alpha = 1e8; % Tradeoff parameter to compute the new subspace

if randProj
    d_arr = randsample((dMin:1:dMax)',iter,true);
else
    d_arr = d*ones(iter,1);
end

%% Training phase
% Compute unweighted adjacency and Laplacian matrices
if size(Xtr, 2) < 4000
    G = L2_distance(Xtr, Xtr);
    % Compute neighbourhood graph
    [tmp, ind] = sort(G);
    for i=1:size(G, 1)
        G(i, ind((2 + k):end, i)) = 0;
    end
    G = sparse(double(G));
    G = max(G, G');             % Make sure distance matrix is symmetric
else
    G = find_nn(Xtr', k);
end

sigma = 1;
G(G ~= 0) = exp(-G(G ~= 0) / (2 * sigma ^ 2));

% Create multiple incoherent subspaces
distMatFull = L2_distance(Xtr,Xtr);
[Vw, errws]  = LPP_incohSubspaces(Xtr,G,d_arr,alpha,0,lambda,distMatFull,0);

% Grassmannian (chordal) mean of subspaces
VGM = weightedMeanChordal(Vw,(1/iter)*ones(iter,1));

% Embedding with the extrinsic mean subspace
YGM = VGM'*Xtr;

% Unsupervised Classification
k = 1;
knnFull = ClassificationKNN.fit(Xtr',labtr,'NumNeighbors',k);
labPred = predict(knnFull,Xte');
knnGM = ClassificationKNN.fit(YGM',labtr,'NumNeighbors',k);
Yte = VGM'*Xte;
labPredGM = predict(knnGM,Yte');

fprintf('Class. Perf. with full data = %.4f \n',sum(labte == labPred)/length(labte));
fprintf('Class. Perf. with Proposed Method = %.4f \n',sum(labte == labPredGM)/length(labte));