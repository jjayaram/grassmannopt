function [Vw, errws]  = LDE_incohSubspaces(X,G,Gp,d_arr,alpha,gamma,lambda,distMatFull,embErr)
% X - data
% G, Gp - intra and inter-class graph
% d_arr - array of dimensions for computing multiple subspaces
% distMatFull - ground truth distance matrix

% Inits
iter = length(d_arr);
errws = zeros(iter,1);
Vw = cell(iter,1);
T = size(X,2);

for i1 = 1:iter
    fprintf('iteration is %d \n',i1);
    
    % Compute the LDE projections of the weighted graphs
    if (i1 == 1)
        Vw{i1} = computeSupProj(X,G,Gp,d_arr(i1));
        C = Vw{i1}*Vw{i1}';
    else
        Vw{i1} = computeSupProjConstr...
                    (X,G,Gp,C,alpha,d_arr(i1));
        C = ((i1-1)/i1)*C+(1/i1)*Vw{i1}*Vw{i1}';
    end
    Yw = Vw{i1}'*X;
    
    % Compute individual embedding errors if needed
    if embErr
        distMatEmbw = L2_distance(Yw,Yw);
        % Compute the embedding error
        errw = errorEmbedding(distMatFull,distMatEmbw,ones(T,1),gamma,lambda);
        
        % Mean error
        errws(i1) = mean(errw);
    end
end