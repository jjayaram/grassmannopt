% Compute the embedding of subspaces computed


cmap = hsv(8);
for i = 1:8
    ids = find(Y==i);
    figure(1); hold on;
    scatter(YLPP(ids,1),YLPP(ids,2),30,cmap(i,:));
    figure(2); hold on;
    scatter(YGMLPP(ids,1),YGMLPP(ids,2),30,cmap(i,:));
    figure(3); hold on;
    scatter(YNPE(ids,1),YNPE(ids,2),30,cmap(i,:));
    figure(4); hold on;
    scatter(YGMNPE(ids,1),YGMNPE(ids,2),30,cmap(i,:));
    figure(5); hold on;
    scatter(YLEM(ids,1),YLEM(ids,2),30,cmap(i,:));
end



