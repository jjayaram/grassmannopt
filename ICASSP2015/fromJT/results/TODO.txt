- Understanding the structure of the grassmannians chosen by looking at their similarities (even doing a 2-D embedding or spectral clustering of it)
- For each i, pick edge in the graph by sampling p_j_i
- Perform ensemble LDE using the averaging method we have
- Perform ensemble LDE by (a) computing projection subspace for the inter-class graph (b) computing projection subspace for intra-class graph mandating that it should be far from that of the inter-class graph


- interclass subspace (ensemble LPP)
- intra class subspace (ensemble LPP, additional constraint far from the subspace in interclass subspace)