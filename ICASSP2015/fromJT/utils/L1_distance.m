function d = L1_distance(a, b)
% L1_DISTANCE - computes L1 distance matrix
%
% E = L2_distance(A,B)
%
%    A - (DxM) matrix 
%    B - (DxN) matrix
% 
% Returns:
%    E - (MxN) L1 distances between vectors in A and B
%
% Example : 
%    A = rand(400,100); B = rand(400,200);
%    d = L1_distance(A,B);

    if nargin < 2
       error('Not enough input arguments');
    end
    if size(a, 1) ~= size(b, 1)
        error('A and B should be of same dimensionality');
    end
    if ~isreal(a) || ~isreal(b)
        warning('Computing distance table using imaginary inputs. Results may be off.'); 
    end

    % Padd zeros if necessray
    if size(a, 1) == 1
        a = [a; zeros(1, size(a, 2))]; 
        b = [b; zeros(1, size(b, 2))]; 
    end
    
    T1 = size(a,2); T2 = size(b,2);
    
    d = zeros(T1,T2);
    
    % Compute distances
    for i2 = 1:T2
        d(:,i2) = sum(abs(bsxfun(@minus,a,b(:,i2))))';
    end
   
    % Make sure result is real
    d = real(d);