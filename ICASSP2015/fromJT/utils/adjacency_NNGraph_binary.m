function [Gu,G,Gp] = adjacency_NNGraph_binary(X,C,k,kp)
% Compute class-unaware (Gu), intraclass (G) and interclass (Gp) adjacency 
% matrices (binary) - VERY fast code.
% X - data
% C - class labels, give all ones if computing only class-unaware
% adjacencies
% k - number of class-unaware and intraclass neighbors
% kp - number of interclass neighbors

N = size(X,2);
I = zeros(N*k*5,1);
Ip = zeros(N*kp*5,1);
Iu = zeros(N*k*5,1);
J = I; Jp = Ip; Ju = Iu;
Count = 1; Countp = 1; Countu = 1;

% Go in jumps of 1000 to speed up the computations
blocksize = 1000;
for j1 = 1:blocksize:N
    blockids = (j1:min(j1+blocksize-1,size(X,2)));
    
    DMatb = L2_distance(X,X(:,blockids));
    
    for i1 = 1:length(blockids)
        DVect = DMatb(:,i1);
        
        % Class unaware
        [sval sind] = sort(DVect,'ascend');
        Iu(Countu:Countu+k) = blockids(i1);
        Ju(Countu:Countu+k) = sind(1:k+1);
        Countu = Countu+k+1;
        
        % Within class
        ind1 = find(C == C(blockids(i1)));
        l1 = numel(ind1);
        if (l1 ~= 0)
            [sval sind] = sort(DVect(ind1),'ascend');
            % Verify if we have a small enough k
            if (k>l1-1), k = l1-1; end
            I(Count:Count+k) = blockids(i1);
            J(Count:Count+k) = ind1(sind(1:k+1));
            Count = Count+k+1;
        end
        
        
        % Across classes
        ind2 = find(C ~= C(blockids(i1)));
        l2 = numel(ind2);
        if (l2 ~= 0)
            [sval sind] = sort(DVect(ind2),'ascend');
            
            % Verify we have a small enough kp
            if (kp > l2), kp = l2; end
            Ip(Countp:Countp+kp-1) = blockids(i1);
            Jp(Countp:Countp+kp-1) = ind2(sind(1:kp));
            Countp = Countp+kp;
        end
    end
end

Count = Count-1;
Countp = Countp-1;
Countu = Countu-1;
I = I(1:Count); Ip = Ip(1:Countp); Iu = Iu(1:Countu);
J = J(1:Count); Jp = Jp(1:Countp); Ju = Ju(1:Countu);

Gu = sparse(Iu,Ju,1,N,N);
G = sparse(I,J,1,N,N);
Gp = sparse(Ip,Jp,1,N,N);

Gu = ((Gu+Gu')>0);
G = ((G+G')>0);
Gp = ((Gp+Gp')>0);