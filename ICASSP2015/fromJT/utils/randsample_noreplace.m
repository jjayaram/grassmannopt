function I=randsample_noreplace(n,k,w)
% Draws k samples from 1:n without replacement according to the weight
% vector w. Should work well (fast) if k << n
I = sort(randsample(n, k, true, w));
while 1
    Idup = find( I(2:end)-I(1:end-1) ==0);
    if isempty(Idup)
            break
    else
            I(Idup)=randsample(n, length(Idup), true, w);
            I = sort(I);
    end
end