% Embed MNIST using t-SNE
addpath(genpath('../Jan5_2015/utils'));

gamma = 0.01:0.04:0.5; % parameter for smoothed error measure
lambda = 0.5; % tradeoff between smoothed precision and recal


load mnist_test
% Pick only 0, 4, 9, 7
inds = find(logical(((test_labels == 10) + (test_labels == 4) +...
    (test_labels == 7) + (test_labels == 9))));

% Pick a 1000 samples from these
rsamp = randperm(length(inds));
inds = inds(rsamp(1:1000));
test_labels = test_labels(inds);
test_X = test_X(inds,:);
ydata = tsne(test_X, test_labels);

load ecoli
ydata = tsne(X',Y);

T = size(X,2);
distMatFull = L2_distance(X,X);
distMatEmb = L2_distance(ydata',ydata');
[errLPP,~, ~] = errorEmbedding(distMatFull,distMatEmb,ones(T,1),gamma,lambda);

[precTSNE, recTSNE] = computePrecRec(X,ydata',50);
figure; plot(recTSNE,precTSNE,'ko--');
