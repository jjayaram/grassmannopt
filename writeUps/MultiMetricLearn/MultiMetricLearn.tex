\documentclass{article} % For LaTeX2e
\usepackage{nips13submit_e,times}
\usepackage{hyperref,amsmath,amssymb}
\usepackage{url}
%\documentstyle[nips13submit_09,times,art10]{article} % For LaTeX 2.09


\title{Learning Multiple Linear Metrics\\ with Soft Data Assignment}


\author{
David S.~Hippocampus\thanks{ Use footnote for providing further information
about author (webpage, alternative address)---\emph{not} for acknowledging
funding agencies.} \\
Department of Computer Science\\
Cranberry-Lemon University\\
Pittsburgh, PA 15213 \\
\texttt{hippo@cs.cranberry-lemon.edu} \\
\And
Coauthor \\
Affiliation \\
Address \\
\texttt{email} \\
\AND
Coauthor \\
Affiliation \\
Address \\
\texttt{email} \\
\And
Coauthor \\
Affiliation \\
Address \\
\texttt{email} \\
\And
Coauthor \\
Affiliation \\
Address \\
\texttt{email} \\
(if needed)\\
}

% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to \LaTeX{} to determine where to break
% the lines. Using \AND forces a linebreak at that point. So, if \LaTeX{}
% puts 3 of 4 authors names on the first line, and the last on the second
% line, try using \AND instead of \And before the third author name.

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}

%\nipsfinalcopy % Uncomment for camera-ready version

\begin{document}


\maketitle

\begin{abstract}
We will describe an approach to learn multiple linear metrics using soft assignments of data to the neighborhood graph induced by each metric. We will also enforce further constraints to ensure that the spectral representation of the induced graphs are orthogonal to each other.
\end{abstract}

\section{Introduction}
Let us assume that we have $T$ input data samples $\{\mathbf{x}_i, \ldots, \mathbf{x}_T\}$ in $\mathbb{R}^D$. Our goal is to learn $C$ metric matrices $\{\mathbf{M}_c\}_{c=1}^C$ that are PSD and $\mathbf{M}_c \in \mathbb{R}^{D \times D}$. We let the matrices have a maximum rank of $d$, and further assume that they can be represented using a global set of basis they can be represented using the basis vectors $\{\mathbf{b}_1, \ldots, \mathbf{b}_K\}$, where $K \gg d$ as 
\begin{equation}
\label{eqn:scm}
\mathbf{M}_c = \sum_{i=1}^K w_{c,i} \mathbf{b}_i \mathbf{b}_i^T,
\end{equation} where $w_{c,i} \geq 0$ and the the vector $\mathbf{w}_c$ is sparse. The set of basis vectors is assumed to be known (either pre-defined or learned from the data). Hence the metric is uniquely defined by the sparse vector $\mathbf{w}_c$. The distance between $\mathbf{x}_i$ and $\mathbf{x}_j$ for a given metric $\mathbf{M}_c$ is represented as 
\begin{equation}
d_c({\mathbf{x}_i,\mathbf{x}_j}) = (\mathbf{x}_i-\mathbf{x}_j)^T \mathbf{M}_c (\mathbf{x}_i-\mathbf{x}_j).
\end{equation} We would like to learn multiple metrics ($\mathbf{w}_c$), using soft assignment of data to each of the metrics such that the neighborhood graphs corresponding to the metrics are incoherent in the spectral domain. 

\textit{Expressing Laplacian eigenmaps through stochastic neighborhoods - similarities and differences.}

\section{Approach}
We will start by defining a stochastic neighborhood in the input domain, using the distances between pairs of points. We use the squared Euclidean distance $d({\mathbf{x}_i,\mathbf{x}_j}) = \|\mathbf{x}_i-\mathbf{x}_j\|_2^2$, and define the probabilities between pairs of points as
\begin{equation}
p_{ij} = \frac{\exp(-\gamma d({\mathbf{x}_i,\mathbf{x}_j}))}
		{\sum_{k} \sum_{l \neq k}\exp(-\gamma d({\mathbf{x}_l,\mathbf{x}_k}))},
		\forall i, \forall j: i \neq j.
\end{equation} This defines the input distribution $P$. 

When we have supervised data with labels given by the function $\lambda(.)$, we will have two input distributions: $P$ that defines the intra-class neighborhood, and $P'$ that defines the inter-class neighborhood. The probabilities for these are defined as
\begin{eqnarray}
p_{ij} &= \frac{\exp(-\gamma d({\mathbf{x}_i,\mathbf{x}_j}))}
		{\sum_{k} \sum_{l \neq k, \lambda(\mathbf{x}_l) = \lambda(\mathbf{x}_k)}\exp(-\gamma d({\mathbf{x}_l,\mathbf{x}_k}))},
		\forall i, \forall j: i \neq j \text{ and } \lambda(\mathbf{x}_i) = \lambda(\mathbf{x}_j)\\
p'_{ij} &= \frac{\exp(-\gamma d({\mathbf{x}_i,\mathbf{x}_j}))}
		{\sum_{k} \sum_{l \neq k, \lambda(\mathbf{x}_l) \neq \lambda(\mathbf{x}_k)}\exp(-\gamma d({\mathbf{x}_l,\mathbf{x}_k}))},
		\forall i, \forall j: i \neq j \text{ and } \lambda(\mathbf{x}_i) \neq \lambda(\mathbf{x}_j)
\end{eqnarray} 

The output distribution $Q$ will be a mixture of $C$ distributions, each defined using the metric $\mathbf{M}_c \forall c$. The pairwise probabilities are defined as
\begin{equation}
\label{eqn:qij}
q_{ij} = \frac{\sum_{c} \pi_i^{(c)} \pi_j^{(c)} \exp(- d_c({\mathbf{x}_i,\mathbf{x}_j}))}
		{\sum_{k} \sum_{l \neq k} \sum_{c} \pi_k^{(c)} \pi_l^{(c)} \exp(- d_c({\mathbf{x}_l,\mathbf{x}_k}))},
		\forall i, \forall j: i \neq j.
\end{equation} Here $\pi_i^{(c)}$ is the probability that the output sample $i$ belongs to the mixture component $c$ and hence $\sum_{c} \pi_i^{(c)} =1, \forall i$. Note that these are used to assign the output data to the mixture in a ``soft'' manner. We denote each (un-normalized) distribution corresponding to the each $c$ value in the numerator of (\ref{eqn:qij}) as $Q_c$, such that $Q = \sum_c Q_c$. Each component in the distribution also defines a graph $G_c$, where the un-normalized edge weight between the nodes $i$ and $j$ are given by $\exp(- d_c({\mathbf{x}_i,\mathbf{x}_j}))$ and the node probabilities are given by $\pi_i^{(c)}$ and $\pi_j^{(c)}$. Let $\mathbf{Q}_c$ denote the adjacency matrix corresponding $G_c$, where the .

In the unsupervised case, the metrics and the node probabilities are obtained by minimizing a distance or divergence measure between $P$ and $Q$. Choosing KL divergence as the measure, the overall optimization can be stated as
\begin{equation}
\label{eqn:unsup_kl}
\min \sum_{i} \sum_{j: j \neq i} p_{ij} \log \left(\frac{p_{ij}}{q_{ij}}\right)
\end{equation} Furthermore, we will constrain that the spectral representation of the matrices $\{\mathbf{Q}_c\}_{c=1}^C$ corresponding to the component graphs $\{G_c\}_{c=1}^C$ are as incoherent as possible. In the supervised case, the same problem can be solved using the intra- and the inter-class stochastic neighborhood distributions as
\begin{equation}
\label{eqn:sup_kl}
\min \sum_{i} \sum_{j: j \neq i} p_{ij} \log \left(\frac{p_{ij}}{q_{ij}}\right) - p'_{ij} \log \left(\frac{p'_{ij}}{q_{ij}}\right).
\end{equation}

In both (\ref{eqn:unsup_kl}) and (\ref{eqn:sup_kl}) the parameters to be optimized include the node probabilities $\pi_i^{(c)}, \forall i, \forall c$ and the sparse vectors $\mathbf{w}_c, \forall c$. 

\textbf{Difference between our formulation and probabilistic metric learning formulations}
\begin{itemize}
\item In the probabilistic formulations, individual softmax functions for each pair of similar and dissimilar points (similar to classification problem), whereas in our approach we construct a single large softmax function for input and output probabilities.
\item In the probabilistic formulations, large number of similar/dissimilar pairs can be handled, whereas in our approach it is much more difficult to handle large sets. In other words they can handle scenarios where similarities and dissimilarities can be encoded in terms of graphs (can we not also do that?)
\end{itemize}


\subsection{Ensuring the Multiple Metrics are Different}
In order to ensure that the metrics are different, we can impose two constraints in several ways. The first way is to mandate that the graphs $G_c$ are different from each other. We can achieve this by imposing that the spectral representation (eigen vectors corresponding to the top eigen values) of $\mathbf{G}_c$ are different from each other. The second way is to ensure that the component distributions $Q_c, \forall c,$ are different from each other or each distribution is different from the sum. The overall optimization problem (in the unsupervised case) can be posed as
\begin{equation}
\label{eqn:unsup_kl_dist_diff}
\min \text{KL} (P || Q) - \sum_{c} \text{KL} (\hat{Q}_c || Q)
\end{equation} where $\hat{Q}_c$ is a normalized version of $Q_c$.


\subsubsection*{References}

\end{document}
