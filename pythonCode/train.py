from sys import argv
import numpy as np
from scipy import linalg
import matplotlib.pyplot as plt
from scipy import stats
import ngl

from common import *
from grassmann import *


if __name__ == '__main__':
    #
    
    [A,D,k] = loadData("/Users/jayaramanthi1/Documents/Project/Personal/Boosted-LDE-Grassmann/data/example/yalefaces_Sub7_Basis.csv");
    [B,D,l] = loadData("/Users/jayaramanthi1/Documents/Project/Personal/Boosted-LDE-Grassmann/data/example/yalefaces_Sub3_Basis.csv");
    
    fname = "/Users/jayaramanthi1/Documents/Project/Personal/Boosted-LDE-Grassmann/data/digits.csv"
    [data,y,nPoints,dim] = loadLabData(fname);
    
    # Build Graphs and perform spectral clustering
    NeighborhoodTypes = ["Ann",
                     "Gabriel",
                     "RelativeNeighbor",
                     "BSkeleton",
                     "Diamond",
                     "RelaxedGabriel",
                     "RelaxedRelativeNeighbor",
                     "RelaxedBSkeleton",
                     "RelaxedDiamond"]
    
    data = np.asanyarray(np.transpose(data),dtype='float32')
    numNeigh = 50;
    edges = ngl.getSymmetricNeighborGraph(NeighborhoodTypes[0],data,numNeigh,1)
    
    Z = np.zeros((nPoints,nPoints))
    for i in range(0,edges.shape[0]):
        Z[edges[i,0],edges[i,1]] = 1
    
    plt.figure()
    plt.imshow(Z, cmap='Greys',  interpolation='nearest')
    plt.colorbar()
    
    K = 10
    Lab1,fea1 = normspecclus(Z,K);
    Lab1 = Lab1 + 1
    Lab1 = bestMap(y,Lab1)
    acc1 = float(np.sum(Lab1==np.reshape(y,(nPoints,1))))/nPoints*100
    
    print acc1
    
    numNeigh = 40;
    edges = ngl.getSymmetricNeighborGraph(NeighborhoodTypes[6],data,numNeigh,1)
    
    Z = np.zeros((nPoints,nPoints))
    for i in range(0,edges.shape[0]):
        Z[edges[i,0],edges[i,1]] = 1
    
    plt.figure()
    plt.imshow(Z, cmap='Greys',  interpolation='nearest')
    plt.colorbar()
    
    K = 10
    Lab2,fea2 = normspecclus(Z,K);
    Lab2 = Lab2 + 1
    Lab2 = bestMap(y,Lab2)
    
    acc2 = float(np.sum(Lab2==np.reshape(y,(nPoints,1))))/nPoints*100
    
    print acc2
        
    if k <= l:
        P,Q = findNearest(A,B)
    else:
        Q,P = findNearest(B,A)
    
    delta1 = findDist(B, P, np.abs(l-k), 'chordal')
    delta2 = findDist(A, Q, np.abs(l-k), 'chordal')

    if np.abs(delta1 - delta2) < 1e-4:
        dist = delta1
    else:
        print "Error - something wrong"
        dist = 0
    
    print k, l, dist
    
    if k == l:
        M = findMidPoint(A,B)
    elif k < l:
        M,N = findGM(A,B)
    else:
        M,N = findGM(B,A)
    
    plt.show()