import numpy as np
from scipy import linalg
from sklearn.cluster import *
from munkres import Munkres

def loadLabData(filename):

    data = np.loadtxt(filename, delimiter=",")
    nSamp, nVar = data.shape
    X = np.transpose(data[:,0:nVar-1])
    y = np.transpose(data[:,nVar-1])
    dim = nVar-1
    return X,y,nSamp,dim

def loadData(filename):

    data = np.loadtxt(filename, delimiter=",")
    nSamp, nVar = data.shape
    dim = nVar
    return data,nSamp,dim

def normspecclus(Z,K):
    nPoints = Z.shape[0]
    Z = np.array(Z)
    D = np.diag(sum(Z,1)**(-0.5))
    D[np.isnan(D)] = 0
    D[np.isinf(D)] = 0
    Lap = np.eye(nPoints) - np.matrix(D)*np.matrix(Z)*np.matrix(D)
    uKS,sKS,vKS = linalg.svd(Lap)
    vKS = np.transpose(vKS)
    f = vKS.shape[1]
    ker = vKS[:,range(f-K,f)]
    for i in range(0,nPoints):
        ker[i,:] = ker[i,:]/linalg.norm(ker[i,:])
    kmobj = KMeans(n_clusters = K)
    L = kmobj.fit_predict(ker)
    
    return L,ker

def bestMap(L1,L2):
    if len(L1)!= len(L2):
        print("ERROR: The two label vectors must be of the same length")
        newL2 = L2
    else:
        L2 = np.reshape(L2, (len(L2),1))
        Lab1 = np.unique(L1)
        nCls1 = len(Lab1)
        Lab2 = np.unique(L2)
        nCls2 = len(Lab2)
        
        nCls = np.max([nCls1, nCls2])
        G = np.zeros((nCls,nCls));
        for i in range(0,nCls1):
            for j in range(0,nCls2):
                dec1 = L1==Lab1[i]
                dec2 = L2==Lab2[j]
                if np.sum(dec1)>0 and np.sum(dec2)>0:
                    dec = np.sum(np.bitwise_and(dec1,dec2))
                    G[i,j] = dec
        m = Munkres()
        indexes = m.compute(-G)
        c = np.zeros(nCls)
        for i in range(0,nCls):
            c[i] = indexes[i][1]

        newL2 = L2*0;
        for i in range(0,nCls2):
            newL2[L2 == Lab2[i]] = Lab1[c[i]];
    
    return newL2
